#include <jni.h>

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    start
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_start
  (JNIEnv * env, jobject obj, jobjectArray args); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    stop
 * Signature: none
 */
JNIEXPORT void JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_stop
  (JNIEnv * env, jobject obj, jstring attribute); { 
 return;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    alg2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_alg2Cpos
  (JNIEnv * env, jobject obj, jstring attribute, jint struc); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    attributeSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_attributeSize
  (JNIEnv * env, jobject obj, jstring attribute); { 
 return 0;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    connect
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_connect
  (JNIEnv * env, jobject obj, jstring user, jstring password); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusAlignementAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusAlignementAttributes
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusCharset
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusCharset
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusFullName
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusFullName
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusInfo
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusInfo
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusPositionalAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusPositionalAttributes
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusProperties
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusProperties
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusStructuralAttributeHasValues
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusStructuralAttributeHasValues
  (JNIEnv * env, jobject obj, jstring attribute); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    corpusStructuralAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_corpusStructuralAttributes
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2Alg
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2Alg
  (JNIEnv * env, jobject obj, jstring attribute, jintArray cpos); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2Id
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2Id
  (JNIEnv * env, jobject obj, jstring attribute, jintArray cpos); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2LBound
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2LBound
  (JNIEnv * env, jobject obj, jstring attribute, jintArray cpos); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2RBound
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2RBound
  (JNIEnv * env, jobject obj, jstring, jintArray); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2Str
  (JNIEnv * env, jobject obj, jstring attribute, jintArray cpos); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cpos2Struc
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cpos2Struc
  (JNIEnv * env, jobject obj, jstring, jintArray); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    cqpQuery
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_cqpQuery
  (JNIEnv * env, jobject obj, jstring motherCorpus, jstring subcorpus, jstring query); { 
 return;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    disconnect
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_disconnect
  (JNIEnv * env, jobject obj); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    dropAttribute
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_dropAttribute
  (JNIEnv * env, jobject obj, jstring attribute); { 
 return;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    dropCorpus
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_dropCorpus
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    dropSubCorpus
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_dropSubCorpus
  (JNIEnv * env, jobject obj, jstring subcorpus); { 
 return;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    dumpSubCorpus
 * Signature: (Ljava/lang/String;BII)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_dumpSubCorpus
  (JNIEnv * env, jobject obj, jstring subcorpus, jbyte field, jint first, jint last); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    fdist1
 * Signature: (Ljava/lang/String;IBLjava/lang/String;)[[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_fdist1
  (JNIEnv * env, jobject obj, jstring subcorpus, jint cutoff, jbyte field, jstring attribute); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    fdist2
 * Signature: (Ljava/lang/String;IBLjava/lang/String;BLjava/lang/String;)[[I
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_fdist2
  (JNIEnv * env, jobject obj, jstring subcorpus , jint cutoff, jbyte field1, jstring attribute1, jbyte field2, jstring attribute2); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    getLastCQPError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_getLastCQPError
  (JNIEnv * env, jobject obj); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    getLastCqiError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_getLastCqiError
  (JNIEnv *env, jobject obj); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    id2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_id2Cpos
  (JNIEnv * env, jobject obj, jstring attribute, jint id); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    id2Freq
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_id2Freq
  (JNIEnv * env, jobject obj, jstring attribute, jintArray ids); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    id2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_id2Str
  (JNIEnv * env, jobject obj, jstring attribute, jintArray ids); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    idList2Cpos
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_idList2Cpos
  (JNIEnv * env, jobject obj, jstring attribute, jintArray ids); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    lexiconSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_lexiconSize
  (JNIEnv * env, jobject obj, jstring attribute); { 
 return 0;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    listCorpora
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_listCorpora
  (JNIEnv * env, jobject obj); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    listSubcorpora
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_listSubcorpora
  (JNIEnv * env, jobject obj, jstring corpus); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    reconnect
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_reconnect
  (JNIEnv * env, jobject obj); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    regex2Id
 * Signature: (Ljava/lang/String;Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_regex2Id
  (JNIEnv * env, jobject obj, jstring Attribute, jstring regex); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    str2Id
 * Signature: (Ljava/lang/String;[Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_str2Id
  (JNIEnv * env, jobject obj, jstring attribute, jobjectArray strings); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    struc2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_struc2Cpos
  (JNIEnv * env, jobject obj, jstring, jint); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    struc2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_struc2Str
  (JNIEnv * env, jobject obj, jstring attribute, jintArray struc); { 
 return NULL;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    subCorpusHasField
 * Signature: (Ljava/lang/String;B)Z
 */
JNIEXPORT jboolean JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_subCorpusHasField
  (JNIEnv * env, jobject obj, jstring subcorpus, jbyte field); { 
 return JNI_TRUE;
}
 

/*
 * Class:     org_txm_searchengine_cqp_LocalCqiClient
 * Method:    subCorpusSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_txm_searchengine_cqp_LocalCqiClient_subCorpusSize
  (JNIEnv * env, jobject obj, jstring subcorpus); { 
 return 0;
}
 

