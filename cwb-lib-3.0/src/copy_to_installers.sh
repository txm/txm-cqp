ARCHDIR="$HOME/workspace441/org.txm.setups/trunk/archs"

cp -rf builds/mingw/* ${ARCHDIR}/win/32bit/cwb/bin && 
cp -rf builds/mingw/* ${ARCHDIR}/win/64bit/cwb/bin/win32 && 
cp -rf builds/mingw64/* ${ARCHDIR}/win/64bit/cwb/bin && 
cp -rf builds/linux-32/* ${ARCHDIR}/debian/32bit/usr/lib/TXM/cwb/bin &&
cp -rf builds/linux-64/* ${ARCHDIR}/debian/64bit/usr/lib/TXM/cwb/bin &&
cp -rf builds/darwin-universal/* ${ARCHDIR}/mac/intel/Applications/TXM/cwb/bin

if [ $? != 0 ]; then
	echo "** $APP: failed to copy binaries of CWB"
	exit 1;
fi
