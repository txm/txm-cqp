#include <glib.h>
#include <stdio.h>

void printHex(gchar* string) {
    char* cp = string;
    for ( ; *cp != '\0'; ++cp ) {
          fprintf(stdout, " %02x", *cp);
    }
    fprintf(stdout, "\n");
cp = string;
    for ( ; *cp != '\0'; ++cp ) {
          fprintf(stdout, "  %c", *cp);
    }
    fprintf(stdout, "\n");
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stdout, "usage: test 'string'\n");
        return 1;
    }


    gchar *s = argv[1];
    
    if (!g_utf8_validate((gchar *)s, -1, NULL)) {
        fprintf(stdout, "Invalid UTF8 string: %s\n", s);
        return 1;
    }

    
    fprintf(stdout, "\nBEFORE=%s\n", s);
    gchar *string = g_utf8_normalize(s, -1, G_NORMALIZE_NFD);
    
    if (string == NULL) {
        fprintf(stdout, "NFD ERROR=%s\n", s);
    } else {
        fprintf(stdout, "NFD=%s\n", string);
    }
    
    gchar *current_char;
    gchar *next_char_begins;
    
    fprintf(stdout, "\nLOOP ON=%s\n", string);
    for (current_char = string; *current_char != '\0'; /* increment is done in-loop */) {
        
        next_char_begins = g_utf8_next_char(current_char);
        //fprintf(stdout, " current char=%s utfchar=%c nextchar=%s utfnextchar=%c\n", current_char, g_utf8_get_char(current_char), next_char_begins, g_utf8_get_char(next_char_begins));
        fprintf(stdout, "\n");
        fprintf(stdout, "current char=%s nextchar=%s\n", current_char, next_char_begins);

            printHex(string);

            if (g_unichar_ismark(g_utf8_get_char(current_char))) {
                  /* downcopy to overwrite the mark character */
                  
                  // g_utf8_strncpy(current_char, next_char_begins, -1);
                  strcpy(current_char, next_char_begins);
                  fprintf(stdout, "*     MARK\n");
                  printHex(string);
                  /* and keep current_char the same */
            }
            else {
                  current_char = next_char_begins;
                  fprintf(stdout, "* NOT MARK\n");
                  printHex(string);
		}
		
    if (!g_utf8_validate(string, -1, NULL)) {
        fprintf(stdout, "Invalid UTF8 string: %s\n", string);
        return 1;
    }

    }
      
    fprintf(stdout, "After loop=%s\n", string);
    
    gchar *string4 = g_utf8_normalize(string, -1, G_NORMALIZE_NFC);
    if (string4 == NULL) {
        fprintf(stdout, "NFC over NFD ERROR=%s\n", string);
    } else {
        fprintf(stdout, "NFC over NFD=%s\n", string4);
    }
    
    return 0;
}    
