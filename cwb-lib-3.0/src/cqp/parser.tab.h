/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ID = 258,
    QID = 259,
    LABEL = 260,
    STRING = 261,
    FLAG = 262,
    TAGSTART = 263,
    TAGEND = 264,
    VARIABLE = 265,
    IPAddress = 266,
    IPSubnet = 267,
    INTEGER = 268,
    DOUBLEFLOAT = 269,
    FIELD = 270,
    FIELDLABEL = 271,
    ANCHORTAG = 272,
    ANCHORENDTAG = 273,
    SEARCH_STRATEGY = 274,
    TAB_SYM = 275,
    CAT_SYM = 276,
    DEFINE_SYM = 277,
    DIFF_SYM = 278,
    DISCARD_SYM = 279,
    EXPAND_SYM = 280,
    EXIT_SYM = 281,
    FLAT_SYM = 282,
    INTER_SYM = 283,
    JOIN_SYM = 284,
    SUBSET_SYM = 285,
    LEFT_SYM = 286,
    RIGHT_SYM = 287,
    SAVE_SYM = 288,
    SCATTER_SYM = 289,
    SHOW_SYM = 290,
    CD_SYM = 291,
    TO_SYM = 292,
    WITHIN_SYM = 293,
    SET_SYM = 294,
    EXEC_SYM = 295,
    CUT_SYM = 296,
    OCCURS_SYM = 297,
    INFO_SYM = 298,
    GROUP_SYM = 299,
    WHERE_SYM = 300,
    ESCAPE_SYM = 301,
    MEET_SYM = 302,
    UNION_SYM = 303,
    MU_SYM = 304,
    SORT_SYM = 305,
    COUNT_SYM = 306,
    ASC_SYM = 307,
    DESC_SYM = 308,
    REVERSE_SYM = 309,
    BY_SYM = 310,
    FOREACH_SYM = 311,
    ON_SYM = 312,
    YES_SYM = 313,
    OFF_SYM = 314,
    NO_SYM = 315,
    SLEEP_SYM = 316,
    REDUCE_SYM = 317,
    MAXIMAL_SYM = 318,
    WITH_SYM = 319,
    WITHOUT_SYM = 320,
    DELETE_SYM = 321,
    SIZE_SYM = 322,
    DUMP_SYM = 323,
    UNDUMP_SYM = 324,
    TABULATE_SYM = 325,
    NOT_SYM = 326,
    CONTAINS_SYM = 327,
    MATCHES_SYM = 328,
    GCDEL = 329,
    APPEND = 330,
    LET = 331,
    GET = 332,
    NEQ = 333,
    IMPLIES = 334,
    RE_PAREN = 335,
    EOL_SYM = 336,
    ELLIPSIS = 337,
    MATCHALL = 338,
    LCSTART = 339,
    LCEND = 340,
    LCMATCHALL = 341,
    PLUSEQ = 342,
    MINUSEQ = 343,
    UNLOCK_SYM = 344,
    USER_SYM = 345,
    HOST_SYM = 346,
    UNDEFINED_MACRO = 347,
    MACRO_SYM = 348,
    RANDOMIZE_SYM = 349,
    FROM_SYM = 350,
    INCLUSIVE_SYM = 351,
    EXCLUSIVE_SYM = 352,
    NULL_SYM = 353
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 120 "parser.y"

  Evaltree           evalt;
  Constrainttree     boolt;
  enum b_ops         boolo;
  int                ival;
  double             fval;
  int                index;
  char              *strval;
  CorpusList        *cl;

  struct {
    int a, b;
  } intpair;

  Context            context;
  ActualParamList   *apl;

  enum ctxtdir       direction;

  struct Redir       redir;

  struct InputRedir  in_redir;

  struct {
    int ok;
    int ival;
    char *cval;
  }                  varval;

  struct {
    FieldType field;
    int inclusive;
  }                  base;

  struct {
    char *variableValue;
    char operator;
  }                  varsetting;

  struct {
    int mindist;
    int maxdist;
  }                  Distance;

  struct {
    FieldType anchor;
    int offset;
  }                  Anchor;

  struct {
    FieldType anchor1;
    int offset1;
    FieldType anchor2;
    int offset2;
  }                  AnchorPair;

  struct {
    char *name;
    int flags;
  }                  AttributeSpecification;

  RangeSetOp         rngsetop;

  SortClause         sortclause;

  FieldType          field;

  SearchStrategy     search_strategy;

  TabulationItem     tabulation_item;

#line 228 "parser.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
