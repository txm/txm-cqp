source build_for_txm.sh

# Linux32
sudo apt-get install --force-yes bison flex libglib2.0-dev libncurses5-dev libreadline-dev openjdk-7-jdk

patchConfig "linux-32" &&
buildCWB "linux-32" &&
copyBuilds "linux-32"


if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to copy binaries for LINUX 32"
	exit 1;
fi

