class CqiJNI { //extends ICqiJNI {
	public native void afficherBonjour();
	public native void afficher(String message);
//	public native byte[] loadFile(String name);

	static {
		String path = System.getProperty(" helloww");
		path += ":/home/mdecorde/workspace37/org.txm.toolbox/cwb/src/CQi/test";
		System.setProperty("java.library.path", path);
		System.out.println("LIB PATH: "+System.getProperty("java.library.path")); 
		System.loadLibrary("CqiJNI");
	}
	
	public static void main(String[] args) {

		if (args.length != 1) { System.out.println("USAGE java ReadFile the_file_path"); return; }

		new CqiJNI().afficherBonjour();
		new CqiJNI().afficher(args[0]);
//
//		byte buf[] = new CqiJNI().loadFile(args[0]);
//		for (int i=0 ; i<buf.length ; i++) {
//			System.out.print((char)buf[i]);
//		}
	}
}
