#include <jni.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "MemCqiServer.h"

#include "server.h"
#include "auth.h"
#include "cqi.h"

#include "../cl/cl.h"
#include "../cl/macros.h"
#include "../cqp/cqp.h"
#include "../cqp/options.h"
#include "../cqp/corpmanag.h"
#include "../cqp/groups.h"

#define ATTHASHSIZE 16384 // the same as in server.c
extern int initialized = 0;

extern char* CQI_CONST_FIELD_MATCH_STR;
extern char* CQI_CONST_FIELD_MATCHEND_STR;
extern char* CQI_CONST_FIELD_TARGET_STR;
extern char* CQI_CONST_FIELD_KEYWORD_STR;

JNIEXPORT jobject JNICALL toBoolean(JNIEnv * env, jobject obj, jboolean bool) {
	jclass cls = (*env)->FindClass(env, "java/lang/Boolean");
	jmethodID mid = (*env)->GetMethodID(env, cls, "<init>", "(Z)V");
	jobject result = (*env)->NewObject(env, cls, mid, bool);
	return result;
}

JNIEXPORT jobject JNICALL toInteger(JNIEnv * env, jobject obj, jint integer) {
	jclass cls = (*env)->FindClass(env, "java/lang/Integer");
	jmethodID mid = (*env)->GetMethodID(env, cls, "<init>", "(I)V");
	jobject result = (*env)->NewObject(env, cls, mid, integer);
	return result;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    start
 * Signature: ([Ljava/lang/String;)Ljava/lang/Boolean;
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_start
(JNIEnv * env, jobject obj, jobjectArray args) {

	printDebugMessage("JNICALL start: start\n");
	jboolean iscopy;
	int argc = (*env)->GetArrayLength(env, args);
	char *argv[argc];
	int i;
	fprintf(stdout,"INIT CQILIB: \n");
	for (i = 0 ; i < argc ; i++) {
		jstring jstr = (jstring)((*env)->GetObjectArrayElement(env, args, i));
		char *str = (*env)->GetStringUTFChars(env, jstr , &iscopy);
		fprintf(stdout," PARAM: %s \n", str);
		if (str == NULL) { return NULL; /* OutOfMemoryError already thrown */}
		argv[i] = str;
		//		(*env)->ReleaseStringChars(env, jstr, str);
	}
	fflush(stdout);
	which_app = cqpserver;
	silent = 1;
	paging = autoshow = auto_save = 0;

	if (!initialize_cqp(argc, argv)) {
		fprintf(stdout, "ERROR Couldn't initialize CQP engine.\n");

		jclass cls = (*env)->FindClass(env, "java/lang/Boolean");
		jmethodID mid = (*env)->GetMethodID(env, cls, "<init>", "(Z)V");
		jobject result = (*env)->NewObject(env, cls, mid, JNI_FALSE);
		return result;
	}
	//	}

	make_attribute_hash(ATTHASHSIZE);
	initialized = 1;

	fprintf(stdout, "END OF START\n");
	jclass cls = (*env)->FindClass(env, "java/lang/Boolean");
	jmethodID mid = (*env)->GetMethodID(env, cls, "<init>", "(Z)V");
	jobject result = (*env)->NewObject(env, cls, mid, JNI_TRUE);
	return result;
	//return toBoolean(env, obj, JNI_TRUE);

}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    load_a_system_corpus
 * Signature: (Ljava/lang/String;)Ljava/lang/Boolean;
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_load_1a_1system_1corpus
(JNIEnv * env, jobject obj, jstring regfilepath, jstring entry) {

    jclass cls = (*env)->FindClass(env, "java/lang/Boolean");
    jmethodID mid = (*env)->GetMethodID(env, cls, "<init>", "(Z)V");

    if (regfilepath == NULL) {
        printDebugMessage("Cqi: load_a_system_corpus: 'regfilepath' is NULL\n");

        jobject result = (*env)->NewObject(env, cls, mid, JNI_FALSE);
        return result;
    }

   	jboolean iscopy;
	char *c = (*env)->GetStringUTFChars(env, regfilepath, &iscopy);
    char *c2 = (*env)->GetStringUTFChars(env, entry, &iscopy);

	if (load_a_system_corpus(c, c2) > 0) {
        jobject result = (*env)->NewObject(env, cls, mid, JNI_TRUE);
        return result;
	} else {
        printDebugMessage("Cqi: load_a_system_corpus: fail to load %s\n", c);
        jobject result = (*env)->NewObject(env, cls, mid, JNI_FALSE);
        return result;
	}
}

JNIEXPORT jobject JNICALL throwException(JNIEnv * env, jobject obj) {
	//fprintf(stdout, "CQPLIB: throwException %d\n", cqi_errno);
	jclass Exception;
	/*switch(cqi_errno) {
	case CQI_ERROR_GENERAL_ERROR: // 0x0201
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiErrorGeneralError");
		break;
	case CQI_ERROR_CONNECT_REFUSED: // 0x0202
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiErrorConnectRefused");
		break;
	case CQI_ERROR_USER_ABORT: // 0x0203
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiErrorUserAbort");
		break;
	case CQI_ERROR_SYNTAX_ERROR: // 0x0204
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiErrorSyntaxError");
		break;

	case CQI_CL_ERROR: //0x04
		break;
	case CQI_CL_ERROR_NO_SUCH_ATTRIBUTE: // 0x0401
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorNoSuchAttribute");
		break;
	case CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE: // 0x0402
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorWrongAttributeType");
		break;
	case CQI_CL_ERROR_OUT_OF_RANGE: // 0x0403
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorOutOfRange");
		break;
	case CQI_CL_ERROR_REGEX: // 0x0404
		// obj.lastError = getLastCqiError();
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorRegex");
		break;
	case CQI_CL_ERROR_CORPUS_ACCESS: // 0x0405
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorCorpusAccess");
		break;
	case CQI_CL_ERROR_OUT_OF_MEMORY: // 0x0406
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorOutOfMemory");
		break;
	case CQI_CL_ERROR_INTERNAL: // 0x0407
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiClErrorInternal");
		break;

	case CQI_CQP_ERROR_GENERAL: // 0x0501
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiCqpErrorErrorGeneral");
		break;
	case CQI_CQP_ERROR_NO_SUCH_CORPUS: // 0x0502
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiCqpErrorNoSuchCorpus");
		break;
	case CQI_CQP_ERROR_INVALID_FIELD: // 0x0503
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiCqpErrorInvalidField");
		break;
	case CQI_CQP_ERROR_OUT_OF_RANGE: // 0x0504
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiCqpErrorOutOfRange");
		break;
	case CQI_CQP_ERROR_SYNTAX: // 0x0505
		// lastError = getLastCQPError();
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiCqpErrorSyntax");
		break;
	case CQI_CQP_UNSUPPORTEDOPERATION: // 0x0506
		// lastError = getLastCQPError();
		Exception = (*env)->FindClass(env, "java/lang/UnsupportedOperationException");
		break;
	default:
		Exception = (*env)->FindClass(env, "org/txm/searchengine/cqp/serverException/CqiErrorGeneralError");
	}*/

	//(*env)->ThrowNew(env, Exception, NULL);
	//fprintf(stdout, "**DEBUG exception thrown\n");
	return NULL;
}

JNIEXPORT jobject JNICALL throwCLException(JNIEnv * env, jobject obj) {
	printDebugMessage("throwCLException: %d\n", cderrno);
	switch (cderrno) {
	case CDA_EATTTYPE:
		cqi_errno = CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE;
		break;
	case CDA_EIDORNG:
	case CDA_EIDXORNG:
	case CDA_EPOSORNG:
		cqi_errno = CQI_CL_ERROR_OUT_OF_RANGE;
		break;
	case CDA_EPATTERN:
	case CDA_EBADREGEX:
		cqi_errno = CQI_CL_ERROR_REGEX;
		strcpy(cqi_error_string, cl_regex_error);
		break;
	case CDA_ENODATA:
		cqi_errno = CQI_CL_ERROR_CORPUS_ACCESS;
		break;
	case CDA_ENOMEM:
		cqi_errno = CQI_CL_ERROR_OUT_OF_MEMORY;
		break;
	case CDA_EOTHER:
	case CDA_ENYI:
		cqi_errno = CQI_CL_ERROR_INTERNAL;
		break;
	case CDA_ECQPSYNTA:
		cqi_errno = CQI_CQP_ERROR_SYNTAX;
		break;
	case CDA_OK:
		fprintf(stdout, "CQPLIB: cderrno == CDA_OK\n");
		//exit(1);
		cqi_errno = CQI_CL_ERROR_INTERNAL;
		break;
	default:
		fprintf(stdout, "CQPLIB: unknown value in cderrno\n");
		//exit(1);
		cqi_errno = CQI_CL_ERROR_INTERNAL;
		break;
	}
	fprintf(stdout, "CQPLIB: throwCLException: call throwException\n");
	return throwException(env, obj);
}

JNIEXPORT jobject JNICALL throwAnException(JNIEnv * env, jobject obj, int error) {
	cqi_errno = error;
	return throwException(env, obj);
}

JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cqi_corpus_attributes(JNIEnv * env, jobject obj, Corpus *c, int type)
{
	Attribute *a;
	int len = 0;
	for (a = first_corpus_attribute(c); a != NULL; a = next_corpus_attribute()) {
		if (a->type == type)
			len++;
	}

	//JNI STUFF
	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}
	jobjectArray result = (*env)->NewObjectArray(env, len, strArrCls, NULL);
	int i = 0;
	for (a = first_corpus_attribute(c); a != NULL; a = next_corpus_attribute()) {
		if (a->type == type) {
			jstring tmp = (*env)->NewStringUTF(env, a->any.name);  /* make sure it is large enough! */
			(*env)->SetObjectArrayElement(env, result, i, tmp);
			(*env)->DeleteLocalRef(env, tmp);
			i++;
		}
	}
	return result;
}


JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_getAttributes(JNIEnv * env, jobject obj, jstring corpus, int type) {
	jobjectArray rez;

	char *c, *typename;
	CorpusList *cl;

	jboolean iscopy;
	c = (*env)->GetStringUTFChars(env, corpus, &iscopy);

	if (server_debug) {
		switch (type) {
		case ATT_POS:
			typename = "POSITIONAL";
			break;
		case ATT_STRUC:
			typename = "STRUCTURAL";
			break;
		case ATT_ALIGN:
			typename = "ALIGNMENT";
			break;
		default:
			printf("INTERNAL ERROR: do_cqi_corpus_attributes(): unknown attribute type");
			return throwAnException(env, obj, CQI_CL_ERROR_WRONG_ATTRIBUTE_TYPE);
		}
		printDebugMessage("JNI CALL CQI_CORPUS_%s_ATTRIBUTES('%s')\n", typename, c);
	}
	cl = findcorpus(c, SYSTEM, 0);
	if (cl == NULL || !access_corpus(cl)) {
		(*env)->ReleaseStringChars(env, corpus, c);
		return throwAnException(env, obj, CQI_CQP_ERROR_NO_SUCH_CORPUS);
	}
	else {
		rez = Java_org_txm_searchengine_cqp_MemCqiServer_cqi_corpus_attributes(env, obj, cl->corpus, type);
	}
	//free(c);
	(*env)->ReleaseStringChars(env, corpus, c);
	return rez;
}

//int
//query_has_semicolon(char *query)
//{
//	char *p;
//
//	if (query == NULL || *query == 0)
//		return 0;
//	p = query + strlen(query);
//	while (--p > query)           /* stop at first non-blank char or at first string character */
//		if (!(*p == ' ' || *p == '\t')) break;
//	return (*p == ';') ? 1 : 0;
//}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    stop
 * Signature: none
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_stop
(JNIEnv * env, jobject obj) {
	printDebugMessage("JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_stop\n");

    free_corpuslist();
    free_attribute_hash();
	//fprintf(stdout, "Loaded corpora: %p",loaded_corpora);
	printDebugMessage("JNICALL END OF Java_org_txm_searchengine_cqp_MemCqiServer_stop\n");
	//	free_corpuslist();
	return toBoolean(env, obj, JNI_TRUE);
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    alg2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_alg2Cpos
(JNIEnv * env, jobject obj, jstring jattribute, jint struc) {
	jintArray result = (*env)->NewIntArray(env, 4);
	int alg = struc, s1, s2, t1, t2;

	Attribute *attribute;

	jboolean iscopy;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_ALG2CPOS('%s', %d)\n", a, alg);
	}
	attribute = cqi_lookup_attribute(a, ATT_ALIGN);
	if (attribute == NULL) {
		return throwException(env, obj);
	} else {
		if (cl_alg2cpos(attribute, alg, &s1, &s2, &t1, &t2)) {
			int ints[] = {s1, s2, t1, t2};
			(*env)->SetIntArrayRegion(env, result, 0, 4, ints);
		} else {
			(*env)->ReleaseStringChars(env, jattribute, a);
			return throwCLException(env, obj);
		}
	}

	(*env)->ReleaseStringChars(env, jattribute, a);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    attributeSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_attributeSize
(JNIEnv * env, jobject obj, jstring jattribute) {

	Attribute *attribute;
	int size = -1;

	jboolean iscopy;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);

	if (server_debug)
		fprintf(stdout, "CQi: CQI_CL_ATTRIBUTE_SIZE('%s')\n", a);
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute != NULL) {
		size = cl_max_cpos(attribute);
		if (size < 0) {
			(*env)->ReleaseStringChars(env, jattribute, a);
			return throwCLException(env, obj);
		}
	} else {
		attribute = cqi_lookup_attribute(a, ATT_STRUC);
		if (attribute != NULL) {
			size = cl_max_struc(attribute);
			if (size < 0) {
				(*env)->ReleaseStringChars(env, jattribute, a);
				return throwCLException(env, obj);
			}
		} else {
			attribute = cqi_lookup_attribute(a, ATT_ALIGN);
			if (attribute != NULL) {
				size = cl_max_alg(attribute);
				if (size < 0) {
					(*env)->ReleaseStringChars(env, jattribute, a);
					return throwCLException(env, obj);
				}
			} else {
				(*env)->ReleaseStringChars(env, jattribute, a);
				return throwException(env, obj);
			}
		}
	}
	//free(a);
	(*env)->ReleaseStringChars(env, jattribute, a);
	return toInteger(env, obj, size);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    connect
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_connect
(JNIEnv * env, jobject obj, jstring user, jstring password) {
	return toBoolean(env, obj, JNI_TRUE);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusAlignementAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusAlignementAttributes
(JNIEnv * env, jobject obj, jstring corpus) {
	return Java_org_txm_searchengine_cqp_MemCqiServer_getAttributes(env, obj, corpus, ATT_ALIGN);
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    getOption
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_getOption
(JNIEnv * env, jobject obj, jstring name) {
	jstring rez;
	jboolean iscopy;
	char *c = (*env)->GetStringUTFChars(env, name, &iscopy);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CORPUS_CHARSET('%s')\n", c);
	}
	/* this is a dummy until we've implemented the registry extensions */
	rez = (*env)->NewStringUTF(env, get_string_option_value(c));

	(*env)->ReleaseStringChars(env, name, c);
	return rez;
}

//char * set_string_option_value(char *opt_name, char *value)
/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    setOption
 * Signature: Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_setOption
(JNIEnv * env, jobject obj, jstring name, jstring value) {
	jstring rez;
	jboolean iscopy;
	jboolean iscopy2;
	char *c = (*env)->GetStringUTFChars(env, name, &iscopy);
	char *c2 = (*env)->GetStringUTFChars(env, value, &iscopy2);

	if (server_debug) {
		fprintf(stdout, "CQi: SET OPTION('%s')\n", c);
	}
	
	rez = (*env)->NewStringUTF(env, set_string_option_value(c, c2));

	(*env)->ReleaseStringChars(env, name, c);
	//(*env)->ReleaseStringChars(env, value, c2); // don't release c2 because it is stored in the cqpoptions table

	return rez;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusCharset
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusCharset
(JNIEnv * env, jobject obj, jstring corpus) {
	jstring rez;
	jboolean iscopy;
	char *c = (*env)->GetStringUTFChars(env, corpus, &iscopy);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CORPUS_CHARSET('%s')\n", c);
	}
	/* this is a dummy until we've implemented the registry extensions */
	rez = (*env)->NewStringUTF(env, "latin1");

	(*env)->ReleaseStringChars(env, corpus, c);
	return rez;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusFullName
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusFullName
(JNIEnv * env, jobject obj, jstring corpus) {
	jstring result;
	CorpusList *cl;
	jboolean iscopy;
	char *c = (*env)->GetStringUTFChars(env, corpus, &iscopy);

	if (server_debug)
		fprintf(stdout, "CQi: CQI_CORPUS_FULL_NAME('%s')\n", c);
	cl = findcorpus(c, SYSTEM, 0);
	if (cl == NULL || !access_corpus(cl)) {
		(*env)->ReleaseStringChars(env, corpus, c);
		return throwAnException(env, obj, CQI_CQP_ERROR_NO_SUCH_CORPUS);
	} else {
		result = (*env)->NewStringUTF(env, cl->corpus->name);
	}

	(*env)->ReleaseStringChars(env, corpus, c);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusInfo
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusInfo
(JNIEnv * env, jobject obj, jstring corpus) {
	return throwAnException(env, obj, CQI_CQP_UNSUPPORTEDOPERATION);
	//return (*env)->NewStringUTF(env, "not suported");
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusPositionalAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusPositionalAttributes
(JNIEnv * env, jobject obj, jstring corpus) {
	return Java_org_txm_searchengine_cqp_MemCqiServer_getAttributes(env, obj, corpus, ATT_POS);
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusProperties
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusProperties
(JNIEnv * env, jobject obj, jstring corpus) {

	jboolean iscopy;
	char *c = (*env)->GetStringUTFChars(env, corpus, &iscopy);
	if (server_debug)
		fprintf(stdout, "CQi: CQI_CORPUS_PROPERTIES('%s')\n", c);
	/* this is a dummy until we've implemented the registry extensions */
	//free(c);
	(*env)->ReleaseStringChars(env, corpus, c);

	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}

	jobjectArray result = (*env)->NewObjectArray(env, 0, strArrCls, NULL);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusStructuralAttributeHasValues
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusStructuralAttributeHasValues
(JNIEnv * env, jobject obj, jstring jattribute) {
	jboolean rez;
	jboolean iscopy;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	Attribute *attribute;

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CORPUS_STRUCTURAL_ATTRIBUTE_HAS_VALUES('%s')\n", a);
	}

	attribute = cqi_lookup_attribute(a, ATT_STRUC);
	if (attribute != NULL) {
		rez = cl_struc_values(attribute);
	} else {
		return throwException(env, obj);
	}

	(*env)->ReleaseStringChars(env, jattribute, a);
	return toBoolean(env, obj, rez);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    corpusStructuralAttributes
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_corpusStructuralAttributes
(JNIEnv * env, jobject obj, jstring corpus) {
	return Java_org_txm_searchengine_cqp_MemCqiServer_getAttributes(env, obj, corpus, ATT_STRUC);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2Alg
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2Alg
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {
	jintArray result;
	jintArray iarr;

	int len, i, alg;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *algs =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2ALG('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_ALIGN);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,algs,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		for (i=0; i<len; i++) {
			algs[i] = cl_cpos2alg(attribute, cposlist[i]);
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,algs,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2Id
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2Id
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {

	jintArray iarr;
	int len, i, id;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *ids =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2ID('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,ids,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		for (i=0; i<len; i++) {
			ids[i] = cl_cpos2id(attribute, cposlist[i]);
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,ids,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2LBound
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2LBound
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {
	jintArray iarr;
	int len, i, struc, lb, rb;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *strucs =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2LBOUND('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_STRUC);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	}
	else {
		for (i=0; i<len; i++) {
			strucs[i] = cl_cpos2struc(attribute, cposlist[i]);
			if (strucs[i] >= 0 && cl_struc2cpos(attribute, strucs[i], &lb, &rb)) {
				strucs[i] = lb;
			}
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2RBound
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2RBound
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {
	jintArray iarr;
	int len, i, struc, lb, rb;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *strucs =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2LBOUND('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_STRUC);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	}
	else {
		for (i=0; i<len; i++) {
			strucs[i] = cl_cpos2struc(attribute, cposlist[i]);
			if (strucs[i] >= 0 && cl_struc2cpos(attribute, strucs[i], &lb, &rb)) {
				strucs[i] = rb;
			}
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2Str
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {

    printDebugMessage("JNICALL cpos2Str\n");
	int len, i;
	char *str;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}
	jobjectArray result = (*env)->NewObjectArray(env, len, strArrCls, NULL);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2STR('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	}
	else {
		for (i=0; i<len; i++) {
			jstring tmp = (*env)->NewStringUTF(env, cl_cpos2str(attribute, cposlist[i]));  /* make sure it is large enough! */
			(*env)->SetObjectArrayElement(env, result, i, tmp);
			(*env)->DeleteLocalRef(env, tmp);
		}
	}

	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cpos2Struc
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cpos2Struc
(JNIEnv * env, jobject obj, jstring jattribute, jintArray cpos) {
	jintArray iarr;
	int len, i, struc;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *cposlist = (*env)->GetIntArrayElements(env, cpos, NULL);
	len = (*env)->GetArrayLength(env, cpos);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *strucs =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_CPOS2STRUC('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", cposlist[i]);
		fprintf(stdout, "])\n");
	}

	attribute = cqi_lookup_attribute(a, ATT_STRUC);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		for (i=0; i<len; i++) {
			strucs[i] = cl_cpos2struc(attribute, cposlist[i]);
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,strucs,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, cpos, cposlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    query
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_query
(JNIEnv * env, jobject obj, jstring jquery) {
	char *query;
	jboolean iscopy;

	query = (*env)->GetStringUTFChars(env, jquery, &iscopy);

	//query_lock = floor(1e9 * cl_runif()) + 1; /* activate query lock mode with random key */

	printf("CQPLIB: start savage mode = %d\n", query_lock);

	if (!cqp_parse_string(query)) { /* parser and execute */
		printf("QUERY ERROR\n");
		(*env)->ReleaseStringChars(env, jquery, query);
		return throwCLException(env, obj);
	} else {
		/*char *full_child = combine_subcorpus_spec(c, child); // c is the 'physical' part of the mother corpus
				CorpusList *childcl = cqi_find_corpus(full_child);

				if ((childcl) == NULL) {
					printf("QUERY ERROR\n");
					return throwCLException(env, obj);
				} else {
					if (server_log) {
						printf("'%s' ran the following query on %s\n", "cqplib", mother);
						printf("\t%s\n", cqp_query);
						printf("and got %d matches.\n", childcl->size);
					}
				}
				free(full_child);*/
	}
	//query_lock = 0;           /* deactivate query lock mode */

	(*env)->ReleaseStringChars(env, jquery, query);
	return toBoolean(env, obj, JNI_TRUE);
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    cqpQuery
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_cqpQuery
(JNIEnv * env, jobject obj, jstring motherCorpus, jstring subcorpus, jstring jquery) {
	char *child, *mother, *query, *c, *sc;
	jboolean iscopy;

	mother = (*env)->GetStringUTFChars(env, motherCorpus, &iscopy);
	child = (*env)->GetStringUTFChars(env, subcorpus, &iscopy);
	query = (*env)->GetStringUTFChars(env, jquery, &iscopy);

	if (server_debug)
		printf("CQi: CQI_CQP_QUERY('%s', '%s', '%s')\n", mother, child, query);

	if (!split_subcorpus_spec(mother, &c, &sc)) {
		(*env)->ReleaseStringChars(env, motherCorpus, mother);
		(*env)->ReleaseStringChars(env, subcorpus, child);
		(*env)->ReleaseStringChars(env, jquery, query);
		return throwException(env, obj);
	} else {
		char *cqp_query;
		int len = strlen(child) + strlen(query) + 10;

		cqp_query = (char *) cl_malloc(len);
		int test1 = check_subcorpus_name(child);
		int test2 = cqi_activate_corpus(mother);
		//printf("tests results: subcorpus_name=%d activation=%d\n", test1, test2);
		if (!test1 || !test2) {
			(*env)->ReleaseStringChars(env, motherCorpus, mother);
			(*env)->ReleaseStringChars(env, subcorpus, child);
			(*env)->ReleaseStringChars(env, jquery, query);
			return throwException(env, obj);
		} else {
			query_lock = floor(1e9 * cl_runif()) + 1; /* activate query lock mode with random key */

			//printf("CQPSERVER: query_lock = %d\n", query_lock);
			if (query_has_semicolon(query))
				sprintf(cqp_query, "%s = %s", child, query);
			else
				sprintf(cqp_query, "%s = %s;", child, query);

			if (server_debug)
				printf("CQi: parsing %s\n", cqp_query);
			if (!cqp_parse_string(cqp_query)) { /* parser and execute */
				fprintf(stdout, "start of throw exeption");
				return throwCLException(env, obj);
				//fprintf(stdout, "End of throw exeption");
			} else {
				char *full_child = combine_subcorpus_spec(c, child); /* c is the 'physical' part of the mother corpus */
				CorpusList *childcl = cqi_find_corpus(full_child);

				if ((childcl) == NULL) {
					(*env)->ReleaseStringChars(env, motherCorpus, mother);
					(*env)->ReleaseStringChars(env, subcorpus, child);
					(*env)->ReleaseStringChars(env, jquery, query);
					return throwCLException(env, obj);
				} else {
					if (server_log) {
						printf("'%s' ran the following query on %s\n", "cqplib", mother);
						printf("\t%s\n", cqp_query);
						printf("and got %d matches.\n", childcl->size);
					}
				}

				if (full_child) cl_free(full_child);
			}
			query_lock = 0;           /* deactivate query lock mode */
		}

		if (cqp_query) cl_free(cqp_query);
	}
	if (c) cl_free(c);
	if (sc) cl_free(sc);


	(*env)->ReleaseStringChars(env, motherCorpus, mother);
	(*env)->ReleaseStringChars(env, subcorpus, child);
	(*env)->ReleaseStringChars(env, jquery, query);
	return toBoolean(env, obj, JNI_TRUE);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    disconnect
 * Signature: ()Z
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_disconnect
(JNIEnv * env, jobject obj) {
	//initialize_cl()
	free_corpuslist();
	return toBoolean(env, obj, JNI_TRUE);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    dropAttribute
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_dropAttribute
(JNIEnv * env, jobject obj, jstring attribute) {
	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_DROP_ATTRIBUTE()  --  not implemented\n");
	}
	return throwAnException(env, obj, CQI_CQP_UNSUPPORTEDOPERATION);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    dropCorpus
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_dropCorpus
(JNIEnv * env, jobject obj, jstring jcorpus) {
    printDebugMessage("JNICALL drop corpus: %s\n", jcorpus);
	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_DROP_CORPUS()\n");
	}
	jboolean iscopy;
	char *name;
	name = (*env)->GetStringUTFChars(env, jcorpus, &iscopy);
	CorpusList* cl = findcorpus(name, SYSTEM, 0);

	if (cl != NULL) {
		printDebugMessage("MemCqiServer.c dropCorpus() call dropcorpus %s\n", name);
		 Attribute *attr = NULL;
		for (attr = cl->corpus->attributes; attr != NULL; attr = attr->any.next) {
            if (attr->any.name != NULL) {
                char * attname = (char *) cl_malloc(4028);
                sprintf(attname, "%s.%s", name, attr->any.name);
                printDebugMessage("cqi_drop_attribute %s\n", attname);
                cqi_drop_attribute(attname);
                free(attname);
            }
		}
		dropcorpus(cl); // remove from corpuslist and call drop_corpus(cl->corpus)
		(*env)->ReleaseStringChars(env, jcorpus, name);
		return toBoolean(env, obj, JNI_TRUE);
	}

	(*env)->ReleaseStringChars(env, jcorpus, name);
	//printf("CQi: CQI_CQP_DROP_CORPUS('%s'): no such corpus\n", name);

	printDebugMessage("corpora\n");
	for (cl = FirstCorpusFromList(); cl != NULL; cl = NextCorpusFromList(cl)) {
		if (cl->type == SYSTEM) {
			printDebugMessage("  %s\n", cl->name);
		}
	}
	return toBoolean(env, obj, JNI_FALSE);
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    dropSubCorpus
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_dropSubCorpus
(JNIEnv * env, jobject obj, jstring jsubcorpus) {
    printDebugMessage("JNICALL dropSubCorpus %s\n", jsubcorpus);
	jboolean iscopy;
	char *subcorpus;
	CorpusList *cl;
	char *c, *sc;

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);
	printDebugMessage("CQI_CQP_DROP_SUBCORPUS('%s')\n", subcorpus);

	// make sure it is a subcorpus, not a root corpus
	if (!split_subcorpus_spec(subcorpus, &c, &sc)) {
		return throwException(env, obj);
	} else if (sc == NULL) {
		if (c) cl_free(c);
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		return throwAnException(env, obj, CQI_ERROR_SYNTAX_ERROR);
	} else {
		if (c) cl_free(c);
		if (sc) cl_free(sc);
		cl = cqi_find_corpus(subcorpus);
		if (cl == NULL) {
			(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
			return throwException(env, obj);
		} else {
            printDebugMessage("MemCqiServer.c dropSubCorpus() call dropcorpus %s\n", subcorpus);
            Attribute *attr = NULL;
            for (attr = cl->corpus->attributes; attr != NULL; attr = attr->any.next) {
            if (attr->any.name != NULL) {
                printDebugMessage("cqi_drop_attribute %s\n", attr->any.name);
                cqi_drop_attribute(attr->any.name);
            }
		}
			dropcorpus(cl);
		}
	}

	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
	return toBoolean(env, obj, JNI_TRUE);
}

void minus_one_list(int* tab, int n) {
	int i;
	for(i = 0 ; i < n ; i++)
		tab[i] = -1;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    dumpSubCorpus
 * Signature: (Ljava/lang/String;BII)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_dumpSubCorpus
(JNIEnv * env, jobject obj, jstring jsubcorpus, jbyte field, jint first, jint last) {
	jintArray iarr = (*env)->NewIntArray(env, 0); // create jintarray
	int *cpos;
	jboolean iscopy;
	char *subcorpus;
	CorpusList *cl;

	//cqi_byte field;
	int i;
	char *fieldname;
	int field_ok = 1;             // field valid?

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);

	//fieldname = cqi_field_name(field);
	char *tmp;
	if (field == CQI_CONST_FIELD_MATCH) {
		tmp = CQI_CONST_FIELD_MATCH_STR;
	} else if (field == CQI_CONST_FIELD_MATCHEND) {
		tmp = CQI_CONST_FIELD_MATCHEND_STR;
	} else if (field == CQI_CONST_FIELD_TARGET) {
		tmp = CQI_CONST_FIELD_TARGET_STR;
	} else if (field == CQI_CONST_FIELD_KEYWORD) {
		tmp = CQI_CONST_FIELD_KEYWORD_STR;
	} else {
		tmp = NULL;                /* invalid field */
	}
	//fprintf(stdout, "tmp: %p", tmp);
	fieldname = tmp;
	if (fieldname == NULL) {
		fieldname = "<invalid field>";
		field_ok = 0;
	}

	if (server_debug) {
		fprintf(stdout, "CQiLib: CQI_CQP_DUMP_SUBCORPUS('%s', %s, %d, %d)\n",
				subcorpus, fieldname, first, last);
	}

	cl = cqi_find_corpus(subcorpus);
	if (cl == NULL) {
		if (iarr) (*env)->ReleasePrimitiveArrayCritical(env, iarr,cpos,0); // unlock the data in jvm
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus); // release param
		return throwException(env, obj);
	} else if (!field_ok) {
		if (iarr) (*env)->ReleasePrimitiveArrayCritical(env, iarr,cpos,0); // unlock the data in jvm
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus); // release param
		return throwAnException(env, obj, CQI_CQP_ERROR_INVALID_FIELD);
	} else if ((last < first) || (first < 0) || (last >= cl->size)) {
		// ERROR // cqi_command(CQI_CQP_ERROR_OUT_OF_RANGE);
		if (iarr) (*env)->ReleasePrimitiveArrayCritical(env, iarr,cpos,0); // unlock the data in jvm
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus); // release param
		return throwAnException(env, obj, CQI_CQP_ERROR_OUT_OF_RANGE);
	} else {
		int size = last - first + 1;
		if (size < 0) size = 0;
		iarr = (*env)->NewIntArray(env, size); // create jintarray
		cpos =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

		int c = 0;
		switch (field) {
		case CQI_CONST_FIELD_MATCH:
			for (i=first; i<=last; i++) {
				cpos[c++] = cl->range[i].start;
			}
			break;
		case CQI_CONST_FIELD_MATCHEND:
			for (i=first; i<=last; i++) {
				cpos[c++] = cl->range[i].end;
			}
			break;
		case CQI_CONST_FIELD_TARGET:
			if (cl->targets == NULL) {
				minus_one_list(cpos, size);
			} else {
				for (i=first; i<=last; i++) {
					cpos[c++] = cl->targets[i];
				}
			}
			break;
		case CQI_CONST_FIELD_KEYWORD:
			if (cl->keywords == NULL) {
				minus_one_list(cpos, size);
			} else {
				for (i=first; i<=last; i++) {
					cpos[c++] = cl->keywords[i];
				}
			}
			break;
		default:
			printf("do_cqi_cqp_dump_subcorpus", "No handler for requested field.");
		}
	}

	//free(subcorpus);

	if (iarr) (*env)->ReleasePrimitiveArrayCritical(env, iarr,cpos,0); // unlock the data in jvm
	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus); // release param
	return iarr;
}

//FieldType field_byte_to_type(

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    fdist1
 * Signature: (Ljava/lang/String;IBLjava/lang/String;)[[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_nfdist1
(JNIEnv * env, jobject obj, jstring jsubcorpus, jint cutoff, jbyte field, jstring jattribute) {
	//fprintf(stdout, "nfdist1: start");
	jintArray iarr;
	jboolean iscopy;
	char *subcorpus;
	CorpusList *cl;
	//int cutoff;
	//cqi_byte field;
	char *att;
	Group *table;
	int i, size;
	char *fieldname;
	FieldType fieldtype = NoField;
	int field_ok = 1;             /* field valid? */

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);
	//	cutoff = cqi_read_int();
	//	field = cqi_read_byte();
	att = (*env)->GetStringUTFChars(env, jattribute, &iscopy);

	//fprintf(stdout, "nfdist1: get field name with field %u \n", field);fflush(stdout);
	/* not exactly the fastest way to do it ... */
	//fieldname = cqi_field_name((cqi_byte)field);
	char *tmp;
	if (field == CQI_CONST_FIELD_MATCH) {
		tmp = CQI_CONST_FIELD_MATCH_STR;
	} else if (field == CQI_CONST_FIELD_MATCHEND) {
		tmp = CQI_CONST_FIELD_MATCHEND_STR;
	} else if (field == CQI_CONST_FIELD_TARGET) {
		tmp = CQI_CONST_FIELD_TARGET_STR;
	} else if (field == CQI_CONST_FIELD_KEYWORD) {
		tmp = CQI_CONST_FIELD_KEYWORD_STR;
	} else {
		tmp = NULL;                /* invalid field */
	}
	//fprintf(stdout, "tmp: %p", tmp);
	fieldname = tmp;
	//printDebugMessage( "sans affectation: %p", cqi_field_name((cqi_byte)field));
	//fprintf(stdout, "%s\n", fieldname);fflush(stdout);
	if (fieldname == NULL) {
		//fprintf(stdout, "nfdist1: get field name: invalid");fflush(stdout);
		fieldname = "<invalid field>";
		field_ok = 0;
	} else {
		//fprintf(stdout, "nfdist1: get field name: ok\n");
		fieldtype = field_name_to_type(fieldname);
		//fprintf(stdout, "nfdist1: after field_name_to_type");
	}
	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CQP_FDIST_1('%s', %d, %s, %s)\n",
				subcorpus, cutoff, fieldname, att);
	}

	//fprintf(stdout, "nfdist1: find subcorpus");
	cl = cqi_find_corpus(subcorpus);
	if (cl == NULL) {
		//fprintf(stdout, "nfdist1: sub corpus not found");
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		(*env)->ReleaseStringChars(env, jattribute, att);
		return throwException(env, obj);
	} else if (!field_ok) {
		//fprintf(stdout, "nfdist1: no field found");
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		(*env)->ReleaseStringChars(env, jattribute, att);
		return throwAnException(env, obj, CQI_CQP_ERROR_INVALID_FIELD);
	} else {
		//fprintf(stdout, "nfdist1: subcorpus ok, compute table");
		/* compute_grouping() returns tokens with f > cutoff, but CQi specifies f >= cutoff */
		cutoff = (cutoff > 0) ? cutoff - 1 : 0;
		table = compute_grouping(cl, NoField, 0, NULL, fieldtype, 0, att, cutoff);

		if (table == NULL) {
			//fprintf(stdout, "nfdist1: table null");
			(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
			(*env)->ReleaseStringChars(env, jattribute, att);
			return throwAnException(env, obj, CQI_CQP_ERROR_GENERAL);
		} else {
			//fprintf(stdout, "nfdist1: table ok");
			size = table->nr_cells;

			iarr = (*env)->NewIntArray(env, 2*size); // create jintarray
			int *ints =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it
			int c = 0;
			for (i=0; i < size; i++) {
				ints[c++] = (table->count_cells[i].t);
				ints[c++] = (table->count_cells[i].freq);
			}

			(*env)->ReleasePrimitiveArrayCritical(env, iarr,ints,0); // unlock the data in jvm
			free_group(&table);
		}
	}

	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
	(*env)->ReleaseStringChars(env, jattribute, att);
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    fdist2
 * Signature: (Ljava/lang/String;IBLjava/lang/String;BLjava/lang/String;)[[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_nfdist2
(JNIEnv * env, jobject obj, jstring jsubcorpus , jint cutoff, jbyte field1, jstring jattribute1, jbyte field2, jstring jattribute2) {
	jintArray iarr;
	jboolean iscopy;
	char *subcorpus;
	CorpusList *cl;
	//int cutoff;
	//cqi_byte field1, field2;
	char *att1, *att2;
	Group *table;
	int i, size;
	char *fieldname1, *fieldname2;
	FieldType fieldtype1 = NoField, fieldtype2 = NoField;
	int fields_ok = 1;            /* (both) fields valid? */

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);
	//cutoff = cqi_read_int();
	//field1 = cqi_read_byte();
	att1 = (*env)->GetStringUTFChars(env, jattribute1, &iscopy);
	//field2 = cqi_read_byte();
	att2 = (*env)->GetStringUTFChars(env, jattribute2, &iscopy);

	/* not exactly the fastest way to do it ... */
	//fieldname1 = cqi_field_name(field1);
	char *tmp;
	if (field1 == CQI_CONST_FIELD_MATCH) {
		tmp = CQI_CONST_FIELD_MATCH_STR;
	} else if (field1 == CQI_CONST_FIELD_MATCHEND) {
		tmp = CQI_CONST_FIELD_MATCHEND_STR;
	} else if (field1 == CQI_CONST_FIELD_TARGET) {
		tmp = CQI_CONST_FIELD_TARGET_STR;
	} else if (field1 == CQI_CONST_FIELD_KEYWORD) {
		tmp = CQI_CONST_FIELD_KEYWORD_STR;
	} else {
		tmp = NULL;                /* invalid field */
	}
	//fprintf(stdout, "tmp: %p", tmp);
	fieldname1 = tmp;
	if (fieldname1 == NULL) {
		fieldname1 = "<invalid field>";
		fields_ok = 0;
	}
	else {
		fieldtype1 = field_name_to_type(fieldname1);
	}
	//fieldname2 = cqi_field_name(field2);
	if (field2 == CQI_CONST_FIELD_MATCH) {
		tmp = CQI_CONST_FIELD_MATCH_STR;
	} else if (field2 == CQI_CONST_FIELD_MATCHEND) {
		tmp = CQI_CONST_FIELD_MATCHEND_STR;
	} else if (field2 == CQI_CONST_FIELD_TARGET) {
		tmp = CQI_CONST_FIELD_TARGET_STR;
	} else if (field2 == CQI_CONST_FIELD_KEYWORD) {
		tmp = CQI_CONST_FIELD_KEYWORD_STR;
	} else {
		tmp = NULL;                /* invalid field */
	}
	//fprintf(stdout, "tmp: %p", tmp);
	fieldname2 = tmp;
	if (fieldname2 == NULL) {
		fieldname2 = "<invalid field>";
		fields_ok = 0;
	} else {
		fieldtype2 = field_name_to_type(fieldname2);
	}

	if (server_debug)
		fprintf(stdout, "CQi: CQI_CQP_FDIST_2('%s', %d, %s, %s, %s, %s)\n",
				subcorpus, cutoff, fieldname1, att1, fieldname2, att2);

	cl = cqi_find_corpus(subcorpus);
	if (cl == NULL) {
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		(*env)->ReleaseStringChars(env, jattribute1, att1);
		(*env)->ReleaseStringChars(env, jattribute2, att2);
		return throwException(env, obj);
	} else if (!fields_ok) {
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		(*env)->ReleaseStringChars(env, jattribute1, att1);
		(*env)->ReleaseStringChars(env, jattribute2, att2);
		return throwAnException(env, obj, CQI_CQP_ERROR_INVALID_FIELD);
	} else {
		/* compute_grouping() returns tokens with f > cutoff, but CQi specifies f >= cutoff */
		cutoff = (cutoff > 0) ? cutoff - 1 : 0;
		table = compute_grouping(cl, fieldtype1, 0, att1, fieldtype2, 0, att2, cutoff);
		if (table == NULL) {
			(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
			(*env)->ReleaseStringChars(env, jattribute1, att1);
			(*env)->ReleaseStringChars(env, jattribute2, att2);
			return throwAnException(env, obj, CQI_CQP_ERROR_GENERAL);
		}
		else {
			size = table->nr_cells;
			iarr = (*env)->NewIntArray(env, 3*size); // create jintarray
			int *ints =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

			int c = 0;
			for (i=0; i < size; i++) {
				ints[c++] = (table->count_cells[i].s);
				ints[c++] = (table->count_cells[i].t);
				ints[c++] = (table->count_cells[i].freq);
			}
			(*env)->ReleasePrimitiveArrayCritical(env, iarr,ints,0); // unlock the data in jvm
			free_group(&table);
		}
	}

	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
	(*env)->ReleaseStringChars(env, jattribute1, att1);
	(*env)->ReleaseStringChars(env, jattribute2, att2);
	return iarr;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    getLastCQPError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_getLastCQPError
(JNIEnv * env, jobject obj) {
	if (*last_cqp_error_message)
		return (*env)->NewStringUTF(env, last_cqp_error_message);
	else
		return (*env)->NewStringUTF(env, "No CQP error message");
	//return (*env)->NewStringUTF(env, "not implemented");
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    getLastCqiError
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_getLastCqiError
(JNIEnv *env, jobject obj) {
	if (*cqi_error_string)
		return (*env)->NewStringUTF(env, cqi_error_string);
	else
		return (*env)->NewStringUTF(env, "No CQi error message");

}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    id2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_id2Cpos
(JNIEnv * env, jobject obj, jstring jattribute, jint id) {

	jintArray iarr;
	int len;
	int *cposlist;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	Attribute *attribute;

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_ID2CPOS('%s', %d)\n", a, id);
	}

	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		cposlist = cl_id2cpos(attribute, id, &len);
		iarr = (*env)->NewIntArray(env, len);
		(*env)->SetIntArrayRegion(env, iarr, 0, len, cposlist);
		if (cposlist) cl_free(cposlist);
	}

	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    id2Freq
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_id2Freq
(JNIEnv * env, jobject obj, jstring jattribute, jintArray ids) {
	jintArray iarr;
	int len, i, f;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *idlist = (*env)->GetIntArrayElements(env, ids, NULL);
	len = (*env)->GetArrayLength(env, ids);
	Attribute *attribute;

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *freqs =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_ID2FREQ('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", idlist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,freqs,0); // unlock the data in jvm
		(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		for (i=0; i < len; i++) {
			freqs[i] = cl_id2freq(attribute, idlist[i]);
			if (freqs[i] < 0) freqs[i] = 0; // return 0 if ID is out of range
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,freqs,0); // unlock the data in jvm
	(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return iarr;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    id2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_id2Str
(JNIEnv * env, jobject obj, jstring jattribute, jintArray ids) {

	int len, i;
	char *str;
	jboolean iscopy;

	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *idlist = (*env)->GetIntArrayElements(env, ids, NULL);
	len = (*env)->GetArrayLength(env, ids);
	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}
	jobjectArray result = (*env)->NewObjectArray(env, len, strArrCls, NULL);
	Attribute *attribute;

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_ID2STR('%s', [", a);
		for (i=0; i<len; i++) {
			fprintf(stdout, "%d ", idlist[i]);
		}
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		for (i=0; i<len; i++) {
			jstring tmp = (*env)->NewStringUTF(env, cl_id2str(attribute, idlist[i]));  /* make sure it is large enough! */
			(*env)->SetObjectArrayElement(env, result, i, tmp);
			(*env)->DeleteLocalRef(env, tmp);
		}
	}

	(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    idList2Cpos
 * Signature: (Ljava/lang/String;[I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_idList2Cpos
(JNIEnv * env, jobject obj, jstring jattribute, jintArray ids) {

	jboolean iscopy;
	jintArray result;
	int *cposlist;
	int i, len, cposlen;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	int *idlist = (*env)->GetIntArrayElements(env, ids, NULL);
	len = (*env)->GetArrayLength(env, ids);
	Attribute *attribute;

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_IDLIST2CPOS('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", idlist[i]);
		fprintf(stdout, "])\n");
	}

	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	} else {
		cposlist = cl_idlist2cpos(attribute, idlist, len, 1, &cposlen);
		result = (*env)->NewIntArray(env, cposlen);
		(*env)->SetIntArrayRegion(env, result, 0, cposlen, cposlist);
	}

	(*env)->ReleaseIntArrayElements(env, ids, idlist, 0); // release param
	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    lexiconSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_lexiconSize
(JNIEnv * env, jobject obj, jstring jattribute) {
	Attribute *attribute;
	int size = -1;

	jboolean iscopy;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);

	if (server_debug)
		printf("CQi: CQI_CL_LEXICON_SIZE('%s')\n", a);
	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute != NULL) {
		size = cl_max_id(attribute);
	} else {
		(*env)->ReleaseStringChars(env, jattribute, a); // release param
		return throwException(env, obj);
	}

	(*env)->ReleaseStringChars(env, jattribute, a); // release param
	return toInteger(env, obj, size);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    listCorpora
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_listCorpora
(JNIEnv * env, jobject obj) {
	CorpusList *cl;
	int n = 0;

	if (server_debug)
		printf("CQi: CQI_CORPUS_LIST_CORPORA()\n");
	/* ugly, but it's easiest ... first count corpora, then return names one by one */
	for (cl = FirstCorpusFromList(); cl != NULL; cl = NextCorpusFromList(cl)) {
		if (cl->type == SYSTEM)
			n++;
	}

	//JNI STUFF
	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}
	jobjectArray result = (*env)->NewObjectArray(env, n, strArrCls, NULL);
	int i = 0;
	for (cl = FirstCorpusFromList(); cl != NULL; cl = NextCorpusFromList(cl)) {
		if (cl->type == SYSTEM) {
			jstring tmp = (*env)->NewStringUTF(env, cl->name);  /* make sure it is large enough! */
			(*env)->SetObjectArrayElement(env, result, i, tmp);
			(*env)->DeleteLocalRef(env, tmp);
			i++;
		}
	}
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    listSubcorpora
 * Signature: (Ljava/lang/String;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_listSubcorpora
(JNIEnv * env, jobject obj, jstring jcorpus) {

	jobjectArray result;
	jboolean iscopy;
	CorpusList *cl, *mother;
	int n = 0;

	char *corpus = (*env)->GetStringUTFChars(env, jcorpus, &iscopy);
	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CQP_LIST_SUBCORPORA(%s)\n", corpus);
	}

	mother = cqi_find_corpus(corpus);
	if (!check_corpus_name(corpus) || mother == NULL) {
		(*env)->ReleaseStringChars(env, jcorpus, corpus);
		return throwException(env, obj);
	} else {
		/* ugly, but it's easiest ... first count corpora, then return names one by one */
		for (cl = FirstCorpusFromList(); cl != NULL; cl = NextCorpusFromList(cl)) {
			if (cl->type == SUB && cl->corpus == mother->corpus)
				n++;
		}

		jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
		if (strArrCls == NULL) {
			return NULL; /* exception thrown */
		}

		result = (*env)->NewObjectArray(env, n, strArrCls, NULL);
		int i = 0;
		for (cl = FirstCorpusFromList(); cl != NULL; cl = NextCorpusFromList(cl)) {
			if (cl->type == SUB && cl->corpus == mother->corpus) {
				jstring tmp = (*env)->NewStringUTF(env, cl->name);  /* make sure it is large enough! */
				(*env)->SetObjectArrayElement(env, result, i, tmp);
				(*env)->DeleteLocalRef(env, tmp);
				i++;
			}
		}
	}

	(*env)->ReleaseStringChars(env, jcorpus, corpus);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    reconnect
 * Signature: ()Z
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_reconnect
(JNIEnv * env, jobject obj) {
	return toBoolean(env, obj, JNI_TRUE);
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    regex2Id
 * Signature: (Ljava/lang/String;Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_regex2Id
(JNIEnv * env, jobject obj, jstring jattribute, jstring jregex) {

	jintArray result;
	int *idlist;
	int len;
	Attribute *attribute;

	jboolean iscopy;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	char *regex = (*env)->GetStringUTFChars(env, jregex, &iscopy);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_REGEX2ID('%s', '%s')\n", a, regex);
	}

	attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleaseStringChars(env, jattribute, a);
		(*env)->ReleaseStringChars(env, jregex, regex);
		return throwException(env, obj);
	} else {
		idlist = cl_regex2id(attribute, regex, 0, &len);
		if (idlist == NULL) {
			result = (*env)->NewIntArray(env, 0);
		} else {
			result = (*env)->NewIntArray(env, len);
			(*env)->SetIntArrayRegion(env, result, 0, len, idlist);
		}
	}

	(*env)->ReleaseStringChars(env, jattribute, a);
	(*env)->ReleaseStringChars(env, jregex, regex);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    str2Id
 * Signature: (Ljava/lang/String;[Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_str2Id
(JNIEnv * env, jobject obj, jstring jattribute, jobjectArray strings) {
	jintArray iarr;
	jboolean iscopy;
	int len, i, id;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	len = (*env)->GetArrayLength(env, strings);
	char* strlist[len];
	for (i = 0 ; i < len ; i++) {
		jobject obj = (*env)->GetObjectArrayElement(env, strings, i);
		strlist[i] = (*env)->GetStringUTFChars(env, (jstring)obj, &iscopy);
	}

	iarr = (*env)->NewIntArray(env, len); // create jintarray
	int *ids =(int *)(*env)->GetPrimitiveArrayCritical(env, iarr,0); // access to the data in jvm heap and locks it

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_STR2ID('%s', [", a);
		for (i=0; i < len; i++)
			fprintf(stdout, "'%s' ", strlist[i]);
		fprintf(stdout, "])\n");
	}

	Attribute *attribute = cqi_lookup_attribute(a, ATT_POS);
	if (attribute == NULL) {
		(*env)->ReleasePrimitiveArrayCritical(env, iarr,ids,0); // unlock the data in jvm
		(*env)->ReleaseStringChars(env, jattribute, a);
		return throwException(env, obj);
	} else {
		for (i=0; i<len; i++) {
			int j = cl_str2id(attribute, strlist[i]);
			if (j < 0) ids[i] = -1;
			else ids[i] = j;
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, iarr,ids,0); // unlock the data in jvm
	(*env)->ReleaseStringChars(env, jattribute, a);
	return iarr;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    struc2Cpos
 * Signature: (Ljava/lang/String;I)[I
 */
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_struc2Cpos
(JNIEnv * env, jobject obj, jstring jstruc, jint n) {

	jboolean iscopy;
	jintArray result = (*env)->NewIntArray(env, 2);
	int struc, start, end;
	char *a = (*env)->GetStringUTFChars(env, jstruc, &iscopy);
	Attribute *attribute;
	struc = n;

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_STRUC2CPOS('%s', %d)\n", a, struc);
	}

	attribute = cqi_lookup_attribute(a, ATT_STRUC);
	if (attribute == NULL) {
		(*env)->ReleaseStringChars(env, jstruc, a);
		return throwException(env, obj);
	} else {
		if (cl_struc2cpos(attribute, struc, &start, &end)) {
			int ints[] = {start, end};
			(*env)->SetIntArrayRegion(env, result, 0, 2, ints);
		} else {
			// ERROR // send_cl_error();
			return throwCLException(env, obj);
		}
	}

	(*env)->ReleaseStringChars(env, jstruc, a);
	return result;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    strucs2Cpos
 * Signature: (Ljava/lang/String;[I)[I
 */
/*
JNIEXPORT jintArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_strucs2Cpos
(JNIEnv * env, jobject obj, jstring jstruc, jintArray strucs) {

	jboolean iscopy;
	int len = (*env)->GetArrayLength(env, strucs);
	jintArray result = (*env)->NewIntArray(env, len * 2);
	int struc, start, end;
	char *a = (*env)->GetStringUTFChars(env, jstruc, &iscopy);
	Attribute *attribute;

	if (attribute == NULL) {
		(*env)->ReleaseStringChars(env, jstruc, a);
		return throwException(env, obj);
	}


	int* struclist = (*env)->GetIntArrayElements(env, struc, NULL);
	for (int i = 0; i < len ; i++) {
		struc = struclist[i];

		if (server_debug) {
			fprintf(stdout, "CQi: CQI_CL_STRUC2CPOS('%s', %d)\n", a, struc);
		}

		attribute = cqi_lookup_attribute(a, ATT_STRUC);
		if (cl_struc2cpos(attribute, struc, &start, &end)) {
			int ints[] = {start, end};
			(*env)->SetIntArrayRegion(env, result, i*2, (i*2)+1, ints);
		} else {
			// ERROR // send_cl_error();
			return throwCLException(env, obj);
		}
	}

	(*env)->ReleaseIntArrayElements(env, strucs, struclist, 0);
	(*env)->ReleaseStringChars(env, jstruc, a);
	return result;
}
*/

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    struc2Str
 * Signature: (Ljava/lang/String;[I)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_struc2Str
(JNIEnv * env, jobject obj, jstring jattribute, jintArray struc) {

	jboolean iscopy;
	int *struclist;
	int len, i;
	char *str;
	Attribute *attribute;
	char *a = (*env)->GetStringUTFChars(env, jattribute, &iscopy);
	struclist = (*env)->GetIntArrayElements(env, struc, NULL);
	len = (*env)->GetArrayLength(env, struc);

	if (server_debug) {
		fprintf(stdout, "CQi: CQI_CL_STRUC2STR('%s', [", a);
		for (i=0; i<len; i++)
			fprintf(stdout, "%d ", struclist[i]);
		fprintf(stdout, "])\n");
	}
	attribute = cqi_lookup_attribute(a, ATT_STRUC);

	jclass strArrCls = (*env)->FindClass(env, "java/lang/String");
	if (strArrCls == NULL) {
		return NULL; /* exception thrown */
	}
	jobjectArray result = (*env)->NewObjectArray(env, len, strArrCls, NULL);

	if (attribute == NULL) {
		(*env)->ReleaseIntArrayElements(env, struc, struclist, 0);
		(*env)->ReleaseStringChars(env, jattribute, a);
		return throwException(env, obj);
	} else {
		for (i = 0; i < len; i++) {
			jstring tmp = (*env)->NewStringUTF(env, cl_struc2str(attribute, struclist[i]));  /* make sure it is large enough! */
			(*env)->SetObjectArrayElement(env, result, i, tmp);
			(*env)->DeleteLocalRef(env, tmp);
		}
	}

	(*env)->ReleaseIntArrayElements(env, struc, struclist, 0);
	(*env)->ReleaseStringChars(env, jattribute, a);
	return result;
}


/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    subCorpusHasField
 * Signature: (Ljava/lang/String;B)Z
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_subCorpusHasField
(JNIEnv * env, jobject obj, jstring jsubcorpus, jbyte field) {

	jboolean rez = JNI_FALSE;
	jboolean iscopy;
	char *subcorpus;
	CorpusList *cl;
	char *fieldname;
	int field_ok = 1;             /* field valid? */

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);

	//fieldname = cqi_field_name(field);
	char *tmp;
	if (field == CQI_CONST_FIELD_MATCH) {
		tmp = CQI_CONST_FIELD_MATCH_STR;
	} else if (field == CQI_CONST_FIELD_MATCHEND) {
		tmp = CQI_CONST_FIELD_MATCHEND_STR;
	} else if (field == CQI_CONST_FIELD_TARGET) {
		tmp = CQI_CONST_FIELD_TARGET_STR;
	} else if (field == CQI_CONST_FIELD_KEYWORD) {
		tmp = CQI_CONST_FIELD_KEYWORD_STR;
	} else {
		tmp = NULL;                /* invalid field */
	}
	//fprintf(stdout, "tmp: %p", tmp);
	fieldname = tmp;
	if (fieldname == NULL) {
		fieldname = "<invalid field>";
		field_ok = 0;
	}
	if (server_debug)
		fprintf(stdout, "CQi: CQI_CQP_SUBCORPUS_HAS_FIELD('%s', %s)\n",
				subcorpus, fieldname);

	cl = cqi_find_corpus(subcorpus);
	if (cl == NULL) {
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		return throwException(env, obj);
	} else if (!field_ok) {
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		return throwAnException(env, obj, CQI_CQP_ERROR_INVALID_FIELD);
	} else {
		switch (field) {
		case CQI_CONST_FIELD_MATCH:
			rez = JNI_TRUE;
			break;
		case CQI_CONST_FIELD_MATCHEND:
			rez = JNI_TRUE;
			break;
		case CQI_CONST_FIELD_TARGET:
			if (cl->targets == NULL) {
				rez = JNI_FALSE;
			} else {
				rez = JNI_TRUE;
			}
			break;
		case CQI_CONST_FIELD_KEYWORD:
			if (cl->keywords == NULL) {
				rez = JNI_FALSE;
			} else {
				rez = JNI_TRUE;
			}
			break;
		default:
			internal_error("do_cqi_cqp_subcorpus_has_field", "Can't identify requested field.");
		}
	}

	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
	return toBoolean(env, obj, rez);
}

JNIEXPORT jint JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_getErrorCode
(JNIEnv * env, jobject obj) {
	return (jint)cqi_errno;
}

/*
 * Class:     org_txm_searchengine_cqp_MemCqiServer
 * Method:    subCorpusSize
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jobject JNICALL Java_org_txm_searchengine_cqp_MemCqiServer_subCorpusSize
(JNIEnv * env, jobject obj, jstring jsubcorpus) {
	jint size = -1;
	char *subcorpus;
	CorpusList *cl;
	jboolean iscopy;

	subcorpus = (*env)->GetStringUTFChars(env, jsubcorpus, &iscopy);
	if (server_debug)
		printf("CQi: CQI_CQP_SUBCORPUS_SIZE('%s')\n", subcorpus);
	cl = cqi_find_corpus(subcorpus);
	if (cl == NULL) {
		(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
		return throwException(env, obj);
	} else {
		size = cl->size;
	}

	(*env)->ReleaseStringChars(env, jsubcorpus, subcorpus);
	return toInteger(env, obj, size);
}
