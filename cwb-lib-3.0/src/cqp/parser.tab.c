/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parser.y"

/*
 *  IMS Open Corpus Workbench (CWB)
 *  Copyright (C) 1993-2006 by IMS, University of Stuttgart
 *  Copyright (C) 2007-     by the respective contributers (see file AUTHORS)
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *  Public License for more details (in the file "COPYING", or available via
 *  WWW at http://www.gnu.org/copyleft/gpl.html).
 */


#include <sys/time.h>
#ifndef __MINGW__
#include <sys/resource.h>
#endif
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>

#include "../cl/globals.h"
#include "../cl/special-chars.h"
#include "../cl/attributes.h"
#include "../cl/macros.h"

#include "cqp.h"
#include "options.h"
#include "ranges.h"
#include "symtab.h"
#include "treemacros.h"
#include "tree.h"
#include "eval.h"
#include "corpmanag.h"
#include "regex2dfa.h"
#include "builtins.h"
#include "groups.h"
#include "targets.h"
#include "attlist.h"
#include "concordance.h"
#include "output.h"
#include "print-modes.h"
#include "variables.h"

#include "parse_actions.h"

/* CQPserver user authentication */
#include "../CQi/auth.h"

/* macro expansion */
#include "macro.h"



/* ============================================================ YACC IF */

extern int yychar;

extern int yylex(void);

void yyerror (char *s)
{
  cqpmessage(Error, "CQP Syntax Error: %s\n\t%s <--", s, QueryBuffer);
  generate_code = 0;
}

void warn_query_lock_violation(void) {
  if (which_app != cqpserver)
    fprintf(stdout, "WARNING: query lock violation attempted\n");
  query_lock_violation++;       /* this is for the CQPserver */
}

/* ============================================================ */

/* note: SYCHRONIZE is a windows API identifier, and it doesn't seem at all
necessary here - it is just defined, then tested.
So: commented out. AH 2/4/2010
#define SYNCHRONIZE
*/

void
synchronize(void)
{
/*#if defined(SYNCHRONIZE)*/
  int macro_status;

  /* delete macro buffers & disable macro expansion while sync'ing */
  delete_macro_buffers(1); /* print stack trace on STDERR */
  macro_status = enable_macros;
  enable_macros = 0;

  if (cqp_input_string != NULL) {
    fprintf(stdout, "Synchronizing to end of line ... \n");
    while (!(yychar <= 0))
      yychar = yylex();
  }
  else {
    fprintf(stdout, "Synchronizing until next ';'...\n");
    while (!(yychar <= 0 || yychar == ';'))
      yychar = yylex();
  }

  enable_macros = macro_status; /* reset enable_macros to previous value */
/*#endif*/
}

#define YYERROR_VERBOSE


#line 189 "parser.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ID = 258,
    QID = 259,
    LABEL = 260,
    STRING = 261,
    FLAG = 262,
    TAGSTART = 263,
    TAGEND = 264,
    VARIABLE = 265,
    IPAddress = 266,
    IPSubnet = 267,
    INTEGER = 268,
    DOUBLEFLOAT = 269,
    FIELD = 270,
    FIELDLABEL = 271,
    ANCHORTAG = 272,
    ANCHORENDTAG = 273,
    SEARCH_STRATEGY = 274,
    TAB_SYM = 275,
    CAT_SYM = 276,
    DEFINE_SYM = 277,
    DIFF_SYM = 278,
    DISCARD_SYM = 279,
    EXPAND_SYM = 280,
    EXIT_SYM = 281,
    FLAT_SYM = 282,
    INTER_SYM = 283,
    JOIN_SYM = 284,
    SUBSET_SYM = 285,
    LEFT_SYM = 286,
    RIGHT_SYM = 287,
    SAVE_SYM = 288,
    SCATTER_SYM = 289,
    SHOW_SYM = 290,
    CD_SYM = 291,
    TO_SYM = 292,
    WITHIN_SYM = 293,
    SET_SYM = 294,
    EXEC_SYM = 295,
    CUT_SYM = 296,
    OCCURS_SYM = 297,
    INFO_SYM = 298,
    GROUP_SYM = 299,
    WHERE_SYM = 300,
    ESCAPE_SYM = 301,
    MEET_SYM = 302,
    UNION_SYM = 303,
    MU_SYM = 304,
    SORT_SYM = 305,
    COUNT_SYM = 306,
    ASC_SYM = 307,
    DESC_SYM = 308,
    REVERSE_SYM = 309,
    BY_SYM = 310,
    FOREACH_SYM = 311,
    ON_SYM = 312,
    YES_SYM = 313,
    OFF_SYM = 314,
    NO_SYM = 315,
    SLEEP_SYM = 316,
    REDUCE_SYM = 317,
    MAXIMAL_SYM = 318,
    WITH_SYM = 319,
    WITHOUT_SYM = 320,
    DELETE_SYM = 321,
    SIZE_SYM = 322,
    DUMP_SYM = 323,
    UNDUMP_SYM = 324,
    TABULATE_SYM = 325,
    NOT_SYM = 326,
    CONTAINS_SYM = 327,
    MATCHES_SYM = 328,
    GCDEL = 329,
    APPEND = 330,
    LET = 331,
    GET = 332,
    NEQ = 333,
    IMPLIES = 334,
    RE_PAREN = 335,
    EOL_SYM = 336,
    ELLIPSIS = 337,
    MATCHALL = 338,
    LCSTART = 339,
    LCEND = 340,
    LCMATCHALL = 341,
    PLUSEQ = 342,
    MINUSEQ = 343,
    UNLOCK_SYM = 344,
    USER_SYM = 345,
    HOST_SYM = 346,
    UNDEFINED_MACRO = 347,
    MACRO_SYM = 348,
    RANDOMIZE_SYM = 349,
    FROM_SYM = 350,
    INCLUSIVE_SYM = 351,
    EXCLUSIVE_SYM = 352,
    NULL_SYM = 353
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 120 "parser.y"

  Evaltree           evalt;
  Constrainttree     boolt;
  enum b_ops         boolo;
  int                ival;
  double             fval;
  int                index;
  char              *strval;
  CorpusList        *cl;

  struct {
    int a, b;
  } intpair;

  Context            context;
  ActualParamList   *apl;

  enum ctxtdir       direction;

  struct Redir       redir;

  struct InputRedir  in_redir;

  struct {
    int ok;
    int ival;
    char *cval;
  }                  varval;

  struct {
    FieldType field;
    int inclusive;
  }                  base;

  struct {
    char *variableValue;
    char operator;
  }                  varsetting;

  struct {
    int mindist;
    int maxdist;
  }                  Distance;

  struct {
    FieldType anchor;
    int offset;
  }                  Anchor;

  struct {
    FieldType anchor1;
    int offset1;
    FieldType anchor2;
    int offset2;
  }                  AnchorPair;

  struct {
    char *name;
    int flags;
  }                  AttributeSpecification;

  RangeSetOp         rngsetop;

  SortClause         sortclause;

  FieldType          field;

  SearchStrategy     search_strategy;

  TabulationItem     tabulation_item;

#line 412 "parser.tab.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   478

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  121
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  137
/* YYNRULES -- Number of rules.  */
#define YYNRULES  325
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  492

#define YYUNDEFTOK  2
#define YYMAXUTOK   353


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   103,     2,     2,     2,   110,   102,     2,
     112,   113,   100,   101,   109,   108,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   119,   104,
     107,   105,   106,   111,   116,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   117,     2,   118,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   114,    99,   115,   120,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   360,   360,   360,   365,   366,   369,   369,   372,   371,
     382,   383,   383,   385,   385,   387,   396,   397,   402,   405,
     406,   407,   407,   411,   412,   413,   414,   415,   416,   417,
     418,   419,   420,   421,   422,   423,   424,   425,   426,   427,
     428,   429,   430,   431,   432,   433,   438,   441,   444,   447,
     454,   458,   459,   466,   471,   479,   480,   486,   492,   495,
     509,   511,   516,   518,   522,   523,   526,   527,   530,   531,
     532,   533,   539,   536,   555,   558,   561,   566,   571,   580,
     586,   592,   603,   604,   605,   606,   609,   635,   643,   646,
     647,   648,   648,   665,   670,   676,   677,   678,   681,   685,
     689,   694,   695,   696,   697,   698,   705,   708,   709,   712,
     718,   724,   733,   734,   740,   739,   744,   743,   749,   751,
     755,   767,   769,   774,   777,   786,   798,   809,   821,   822,
     825,   843,   844,   848,   852,   853,   854,   857,   858,   863,
     867,   871,   875,   881,   882,   885,   891,   897,   905,   906,
     911,   915,   916,   920,   922,   926,   930,   931,   935,   936,
     941,   952,   953,   963,   964,   965,   968,   973,   977,   978,
     981,   982,   984,   981,   988,   990,   993,   995,   998,  1006,
    1011,  1016,  1021,  1024,  1025,  1030,  1035,  1042,  1047,  1056,
    1057,  1060,  1061,  1063,  1066,  1067,  1068,  1069,  1072,  1078,
    1079,  1082,  1083,  1086,  1087,  1090,  1091,  1103,  1104,  1112,
    1113,  1121,  1152,  1155,  1156,  1161,  1160,  1166,  1169,  1170,
    1173,  1181,  1189,  1190,  1193,  1194,  1197,  1198,  1201,  1202,
    1205,  1211,  1218,  1219,  1220,  1223,  1224,  1227,  1228,  1232,
    1252,  1253,  1254,  1255,  1256,  1257,  1260,  1261,  1270,  1273,
    1274,  1277,  1278,  1281,  1282,  1283,  1284,  1285,  1286,  1289,
    1290,  1291,  1295,  1312,  1320,  1330,  1331,  1332,  1333,  1334,
    1335,  1338,  1341,  1343,  1360,  1371,  1374,  1375,  1376,  1384,
    1391,  1397,  1398,  1406,  1412,  1418,  1422,  1426,  1429,  1430,
    1432,  1433,  1438,  1438,  1440,  1441,  1442,  1445,  1446,  1450,
    1452,  1456,  1465,  1475,  1481,  1482,  1486,  1489,  1493,  1502,
    1512,  1516,  1517,  1520,  1525,  1526,  1528,  1529,  1531,  1532,
    1534,  1535,  1537,  1538,  1542,  1543
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "QID", "LABEL", "STRING", "FLAG",
  "TAGSTART", "TAGEND", "VARIABLE", "IPAddress", "IPSubnet", "INTEGER",
  "DOUBLEFLOAT", "FIELD", "FIELDLABEL", "ANCHORTAG", "ANCHORENDTAG",
  "SEARCH_STRATEGY", "TAB_SYM", "CAT_SYM", "DEFINE_SYM", "DIFF_SYM",
  "DISCARD_SYM", "EXPAND_SYM", "EXIT_SYM", "FLAT_SYM", "INTER_SYM",
  "JOIN_SYM", "SUBSET_SYM", "LEFT_SYM", "RIGHT_SYM", "SAVE_SYM",
  "SCATTER_SYM", "SHOW_SYM", "CD_SYM", "TO_SYM", "WITHIN_SYM", "SET_SYM",
  "EXEC_SYM", "CUT_SYM", "OCCURS_SYM", "INFO_SYM", "GROUP_SYM",
  "WHERE_SYM", "ESCAPE_SYM", "MEET_SYM", "UNION_SYM", "MU_SYM", "SORT_SYM",
  "COUNT_SYM", "ASC_SYM", "DESC_SYM", "REVERSE_SYM", "BY_SYM",
  "FOREACH_SYM", "ON_SYM", "YES_SYM", "OFF_SYM", "NO_SYM", "SLEEP_SYM",
  "REDUCE_SYM", "MAXIMAL_SYM", "WITH_SYM", "WITHOUT_SYM", "DELETE_SYM",
  "SIZE_SYM", "DUMP_SYM", "UNDUMP_SYM", "TABULATE_SYM", "NOT_SYM",
  "CONTAINS_SYM", "MATCHES_SYM", "GCDEL", "APPEND", "LET", "GET", "NEQ",
  "IMPLIES", "RE_PAREN", "EOL_SYM", "ELLIPSIS", "MATCHALL", "LCSTART",
  "LCEND", "LCMATCHALL", "PLUSEQ", "MINUSEQ", "UNLOCK_SYM", "USER_SYM",
  "HOST_SYM", "UNDEFINED_MACRO", "MACRO_SYM", "RANDOMIZE_SYM", "FROM_SYM",
  "INCLUSIVE_SYM", "EXCLUSIVE_SYM", "NULL_SYM", "'|'", "'*'", "'+'", "'&'",
  "'!'", "';'", "'='", "'>'", "'<'", "'-'", "','", "'%'", "'?'", "'('",
  "')'", "'{'", "'}'", "'@'", "'['", "']'", "':'", "'~'", "$accept",
  "line", "$@1", "command", "$@2", "$@3", "$@4", "$@5", "CorpusCommand",
  "UnnamedCorpusCommand", "CYCommand", "$@6", "InteractiveCommand",
  "EOLCmd", "Cat", "Saving", "OptionalRedir", "Redir",
  "OptionalInputRedir", "InputRedir", "Showing", "AttributeSelections",
  "AttributeSelection", "CorpusSetExpr", "SetOp", "SubsetExpr", "$@7",
  "Discard", "DiscArgs", "VarPrintCmd", "VarDefCmd", "VariableValueSpec",
  "OptionSetCmd", "$@8", "OptBase", "InclusiveExclusive", "VarValue",
  "ExecCmd", "InfoCmd", "GroupCmd", "OptExpansion", "TabulateCmd", "$@9",
  "$@10", "TabulationItems", "TabulationItem", "TabulationRange",
  "OptAttributeSpec", "SortCmd", "OptionalSortClause", "SortClause",
  "SortBoundaries", "SortDirection", "OptReverse", "Reduction",
  "OptPercent", "Delete", "LineRange", "SleepCmd", "SizeCmd", "DumpCmd",
  "UndumpCmd", "OptAscending", "OptWithTargetKeyword", "Query", "AQuery",
  "StandardQuery", "MUQuery", "OptKeep", "SearchPattern", "$@11", "$@12",
  "$@13", "RegWordfExpr", "RegWordfTerm", "RegWordfFactor",
  "RegWordfPower", "Repeat", "AnchorPoint", "XMLTag", "RegexpOp",
  "NamedWfPattern", "OptTargetSign", "OptRefId", "WordformPattern",
  "ExtConstraint", "LookaheadConstraint", "OptionalFlag",
  "GlobalConstraint", "AlignmentConstraints", "$@14", "OptNot",
  "SearchSpace", "CutStatement", "OptNumber", "OptInteger", "OptMaxNumber",
  "ReStructure", "OptDirection", "Description", "OptionalCID", "CID",
  "BoolExpr", "RelExpr", "MvalOp", "OptionalNot", "RelLHS", "RelRHS",
  "RelOp", "FunctionCall", "FunctionArgList", "SingleArg",
  "LabelReference", "MUStatement", "MeetStatement", "MeetContext",
  "UnionStatement", "TABQuery", "TabPatterns", "TabOtherPatterns",
  "OptDistance", "AuthorizeCmd", "$@15", "OptionalGrants", "Grants",
  "Macro", "OptDEFINE_SYM", "ShowMacro", "MultiString", "RandomizeCmd",
  "OtherCommand", "OptionalFIELD", "OptON", "OptFROM", "OptTO",
  "OptELLIPSIS", "Anchor", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   124,
      42,    43,    38,    33,    59,    61,    62,    60,    45,    44,
      37,    63,    40,    41,   123,   125,    64,    91,    93,    58,
     126
};
# endif

#define YYPACT_NINF (-300)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-324)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      23,  -300,    73,   244,  -300,  -300,  -300,    98,  -300,   178,
     329,    31,    13,  -300,    28,  -300,  -300,  -300,   117,  -300,
      56,  -300,   113,    44,  -300,   117,  -300,  -300,   188,   180,
     117,   117,    46,   162,   168,   117,   117,   117,  -300,   117,
     117,   197,   117,   117,   134,   117,   220,   117,   238,    57,
     233,   147,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,   170,  -300,  -300,  -300,  -300,   156,   188,  -300,
     226,  -300,  -300,   201,  -300,   153,    12,  -300,   232,  -300,
    -300,  -300,    36,  -300,   117,   -27,    39,   -29,   117,  -300,
     -27,  -300,  -300,  -300,   275,   288,   297,    63,  -300,   155,
     286,  -300,   290,  -300,   289,  -300,    15,   253,  -300,   270,
     102,  -300,   300,    39,   254,   302,   313,  -300,  -300,  -300,
    -300,  -300,    35,  -300,  -300,   305,  -300,  -300,   285,  -300,
    -300,   318,   293,   317,  -300,  -300,    10,  -300,   151,    10,
    -300,  -300,  -300,   223,  -300,  -300,   253,     9,    45,  -300,
    -300,  -300,    36,   240,    24,  -300,   175,  -300,  -300,  -300,
    -300,   336,   339,  -300,  -300,  -300,  -300,   333,   167,   215,
     224,   341,  -300,  -300,  -300,   236,  -300,  -300,  -300,  -300,
    -300,   346,  -300,  -300,  -300,  -300,  -300,    21,   342,   235,
     351,   354,   345,  -300,  -300,   322,    62,   283,   352,   356,
    -300,  -300,  -300,  -300,   353,   359,   324,   289,   287,  -300,
     266,   375,  -300,   372,    49,  -300,    18,   201,  -300,  -300,
    -300,   274,  -300,  -300,    10,    10,   252,   -14,  -300,   330,
    -300,  -300,    12,    12,    70,  -300,   322,  -300,   374,   385,
     223,  -300,  -300,  -300,  -300,   383,  -300,   186,   -36,    36,
     319,  -300,  -300,  -300,  -300,   379,  -300,  -300,  -300,   357,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,   387,  -300,  -300,
    -300,  -300,  -300,   391,   164,   317,  -300,  -300,   -27,   295,
     337,   396,  -300,  -300,   357,   397,  -300,   304,    87,  -300,
     410,     2,   401,   303,   176,  -300,    29,   413,   414,  -300,
      34,   153,  -300,   372,     6,  -300,   -19,  -300,  -300,    10,
    -300,    10,    10,  -300,  -300,  -300,  -300,  -300,  -300,   412,
       6,    12,    12,  -300,  -300,  -300,  -300,  -300,   317,  -300,
    -300,  -300,    24,    10,  -300,    -9,  -300,   408,   309,    29,
     306,   289,   289,   400,    40,  -300,  -300,  -300,  -300,  -300,
     415,  -300,   420,  -300,  -300,   289,  -300,   317,  -300,  -300,
     289,   357,  -300,  -300,   314,   316,  -300,  -300,    94,   417,
    -300,  -300,   317,  -300,  -300,  -300,   421,  -300,  -300,   122,
    -300,  -300,   138,   331,  -300,   317,  -300,    59,   321,   335,
     326,   125,   293,   426,  -300,   -27,  -300,   402,  -300,   438,
     439,  -300,   -27,  -300,   228,   289,   -27,  -300,  -300,  -300,
    -300,   430,     8,   440,   440,   426,  -300,   332,  -300,   338,
       6,  -300,  -300,  -300,   431,   340,  -300,  -300,  -300,  -300,
    -300,  -300,   334,  -300,   201,   322,   322,  -300,  -300,  -300,
     394,     2,  -300,   289,  -300,  -300,  -300,   444,   444,   343,
    -300,   317,  -300,  -300,  -300,  -300,  -300,   432,   400,   400,
    -300,  -300,   289,    87,  -300,  -300,  -300,  -300,   449,   -27,
     -27,  -300,  -300,   360,  -300,  -300,   441,  -300,   200,  -300,
    -300,  -300
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       2,     4,     0,     0,     1,    15,    46,     0,     3,    21,
     305,     0,     0,     8,   239,    71,    70,    68,   238,    69,
       0,    16,   231,   170,    20,     0,    67,    19,   238,   304,
       0,   238,    58,    88,     0,   238,   108,     0,   313,   238,
     238,     0,   238,   238,     0,     0,     0,     0,     0,     0,
     311,     0,    24,    25,    23,    26,    31,    30,    29,    32,
      33,    34,    40,    35,    28,    27,    36,    37,    38,    39,
      41,    42,     0,    43,    44,    14,    10,     0,    21,   239,
       0,   237,     7,   234,    18,   200,     0,    22,   161,   163,
     164,   217,   200,   165,     0,    52,    52,     0,    74,    76,
      52,    59,    77,    61,   306,     0,     0,    60,    63,    87,
       0,   106,     0,   107,     0,    45,   129,     0,   150,     0,
       0,   152,   315,    52,   158,   116,     0,   294,   295,   296,
     312,    12,     0,     9,    17,     0,   232,   233,     0,   199,
     287,   202,   221,   212,   206,   208,     0,   210,     0,     0,
     278,   203,   204,   169,   276,   277,   129,   223,   252,   193,
     189,   190,   200,   171,   175,   177,   182,   186,   185,   184,
      66,     0,     0,    49,    51,   318,    47,     0,     0,     0,
       0,     0,    78,    75,    50,   307,    64,    65,    62,   100,
      99,    98,   101,   102,   103,   104,    86,     0,   141,   324,
       0,     0,   227,   125,   128,   223,     0,   148,     0,     0,
     147,   314,   151,   153,     0,     0,   157,     0,   319,   292,
       0,     0,    72,   225,   291,   201,     0,   234,   284,   211,
     205,   255,   275,   257,     0,     0,     0,     0,   245,   248,
     258,   253,     0,     0,     0,   168,   223,   162,     0,     0,
     169,   251,   195,   196,   191,     0,   194,     0,     0,   200,
     214,   176,   179,   180,   181,     0,   178,    54,    53,   321,
      84,    79,    85,    80,    83,    81,    82,     0,   105,    89,
      91,    90,   142,     0,   223,   212,   226,   126,    52,   144,
       0,     0,   145,   146,   321,   159,   156,    56,    52,   118,
     124,   121,     0,   298,     0,   303,     0,   236,     0,   230,
       0,   200,   198,   225,     0,   244,     0,   256,   254,     0,
     209,     0,     0,   269,   270,   268,   267,   266,   265,     0,
       0,     0,     0,   207,   167,   222,   215,   166,   212,   249,
     250,   183,   174,     0,   172,     0,   320,     0,     0,     0,
       0,     0,     0,   113,   133,   127,   143,   139,   140,   149,
       0,   160,     0,   155,    55,     0,   115,   212,   120,   322,
       0,   321,   300,   293,     0,     0,    73,   235,     0,     0,
     286,   220,   212,   262,   263,   264,     0,   259,   274,     0,
     272,   243,   240,   241,   242,   212,   246,   282,     0,   219,
       0,   213,   221,   229,   187,    52,   308,     0,   325,     0,
       0,   112,    52,   316,   136,     0,    52,    57,   119,   123,
     122,     0,     0,     0,     0,   229,   288,     0,   260,     0,
       0,   271,   247,   281,     0,     0,   283,   218,   170,   192,
     173,   228,     0,    48,   234,   223,   223,   111,   134,   135,
     138,   131,   154,     0,   299,   297,   310,   302,   301,     0,
     290,   212,   273,   280,   279,   216,   188,   225,   113,   113,
     137,   130,     0,    52,   309,   289,   261,   224,     0,    52,
      52,   132,   117,    94,   109,   110,     0,    92,    97,    95,
      96,    93
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,   376,
    -300,  -300,  -300,  -300,  -300,  -300,   -96,  -300,  -300,  -300,
    -300,  -300,   350,   433,  -300,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,
    -170,  -300,  -300,  -300,     7,    97,  -300,  -300,  -300,   307,
     347,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,   209,    27,
    -300,  -300,  -300,   308,   207,  -157,  -300,  -300,  -300,  -300,
    -300,   -82,  -300,  -300,   241,  -260,  -300,  -277,  -300,  -300,
    -300,  -300,    66,  -203,     4,  -300,    47,  -300,  -222,   160,
     301,   150,  -143,  -300,   230,  -300,  -299,   144,  -300,  -300,
    -300,    48,   239,  -176,  -300,  -300,  -300,  -300,  -300,  -300,
    -300,  -300,  -300,  -300,  -300,  -300,  -300,  -300,    52,  -300,
    -300,  -300,  -300,   -86,  -268,    26,  -113
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     8,     9,    77,    10,    11,    20,    21,
      22,    23,    51,    12,    52,    53,   173,   174,   363,   364,
      54,   107,   108,    24,    25,    26,   306,    55,    98,    56,
      57,   182,    58,   349,   487,   491,   196,    59,    60,    61,
     412,    62,   217,   218,   298,   299,   300,   368,    63,   203,
     204,   414,   450,   471,    64,   357,    65,   210,    66,    67,
      68,    69,   297,   216,    87,    88,    89,    90,   246,    91,
      92,   260,   402,   163,   164,   165,   166,   266,   167,   168,
     255,   169,   141,   226,   150,   151,   152,   230,   344,   157,
     399,   438,   228,   250,   308,   287,   442,    84,   138,   309,
      80,    81,   237,   238,   256,   257,   239,   388,   330,   240,
     389,   390,   241,   153,   154,   435,   155,    93,   142,   224,
     311,    70,   303,   373,   422,    71,    72,    73,   457,    74,
     115,   212,   415,   177,   347,   370,   301
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     176,   200,   288,   140,   184,   313,   244,   261,   354,   231,
     232,   454,   382,   231,   232,   387,   383,  -323,   143,   384,
     385,   233,   144,    -5,   143,   233,   360,   213,   144,  -200,
    -200,   387,   158,   159,  -200,   143,   279,   214,   220,   144,
     280,   160,   161,   334,   158,   159,   376,   378,   171,   101,
     248,  -197,  -319,   160,   161,  -317,   102,    75,   178,   179,
     319,   400,   433,   259,    85,   319,   331,   332,   127,   128,
     201,   320,   434,     4,  -285,   289,   180,   341,   181,   172,
     321,   353,   103,   322,   369,   321,   386,  -285,   322,   407,
     419,   315,   316,    86,   391,   145,   146,   413,   147,  -285,
     403,   145,   146,   421,   147,   428,   404,  -200,  -200,   202,
    -200,    13,   145,   234,   171,   207,   251,    76,   432,   281,
      79,   455,   235,   252,   148,   290,   236,     1,   249,   149,
     236,   387,   302,    78,   175,   149,   162,    79,    83,   104,
     139,  -200,   221,   379,   121,   172,   149,   105,   162,   319,
     253,   254,   139,  -285,   106,   397,   398,   129,   189,    27,
      82,   190,   171,   310,   105,   109,   208,   209,   191,   321,
    -239,   106,   322,   270,   111,    94,   392,   271,   393,   394,
      99,    14,   374,   110,   476,   261,   113,   114,   333,   375,
      97,    79,   355,   172,   122,   123,   365,   125,   242,   243,
     401,    15,   366,   425,   319,   248,    16,    17,    18,   426,
     118,    15,   192,   193,   194,   195,    16,    17,    18,   351,
     352,   272,   467,   124,   321,   273,    19,   322,    27,   380,
     274,   430,   136,   137,   275,   431,    19,   321,   409,   410,
     322,   126,   468,   469,   170,     5,   130,    -6,   183,    -6,
      -6,   131,    -6,    -6,    -6,   317,   232,   420,   339,   340,
     133,    -6,    -6,   132,    -6,   -11,   -11,    -6,   -11,   139,
     -13,   135,    -6,    -6,    -6,   262,   263,   -11,   185,   -11,
     448,   449,   156,   -11,   -11,   -11,   264,   -11,   -11,   265,
     -11,   186,    -6,    -6,   -11,   -11,   489,   490,   479,   480,
     187,   197,   451,   198,   199,   -11,   -11,   206,   201,   443,
     -11,   -11,   -11,   -11,   -11,   211,   447,  -114,   215,   219,
     452,   222,   223,   225,   229,     6,   245,    -6,    -6,    96,
      -6,   227,   100,     7,   -11,   -11,   112,   -11,   -11,   259,
     116,   117,   267,   119,   120,   268,   269,   276,   277,   278,
      28,    29,   283,    30,   284,   282,    -6,   285,   286,   481,
      -6,    -6,    31,   248,    32,   291,   294,   292,    33,    34,
      35,   293,    36,    37,   295,    38,   296,   482,   304,    39,
      40,   305,   175,   484,   485,   307,   314,   335,   336,   338,
      41,    42,   345,   343,   346,    43,    44,    45,    46,    47,
     348,   251,  -252,  -252,   350,   356,   323,   324,   325,   359,
     358,   362,   361,   367,   371,   372,  -224,   377,   395,    48,
      49,   405,   406,    50,   408,   411,   417,   423,   416,   424,
     427,   429,   439,   322,   436,   326,   327,   328,   437,   441,
     444,   445,   446,   453,   463,   477,   456,   460,   470,   466,
     474,   461,   483,   464,   134,   486,   488,   188,   475,   337,
     473,    95,   418,   247,   205,   465,   342,   312,   440,   329,
     258,   478,   459,   381,   396,   318,   458,   472,   462
};

static const yytype_int16 yycheck[] =
{
      96,   114,   205,    85,   100,   227,   149,   164,   285,     3,
       4,     3,     6,     3,     4,   314,    10,    15,     6,    13,
      14,    15,    10,     0,     6,    15,   294,   123,    10,     5,
       6,   330,     8,     9,    10,     6,    15,   123,     3,    10,
      19,    17,    18,   246,     8,     9,   306,    13,    75,     3,
      41,     6,    13,    17,    18,    15,    10,    26,    87,    88,
      79,   338,     3,    99,    20,    79,   242,   243,    11,    12,
      55,    85,    13,     0,    25,    13,   105,   113,   107,   106,
      99,   284,    36,   102,    82,    99,    80,    38,   102,   349,
     367,   234,   235,    49,   113,    83,    84,    57,    86,    50,
     109,    83,    84,   371,    86,   382,   115,    83,    84,    94,
      86,    13,    83,   103,    75,    13,    71,   104,   395,    98,
       3,   113,   112,    78,   112,    63,   120,   104,   119,   117,
     120,   430,   218,   105,    95,   117,   112,     3,    25,    93,
     116,   117,   107,   109,    10,   106,   117,   101,   112,    79,
     105,   106,   116,   104,   108,   331,   332,   100,     3,     9,
     104,     6,    75,   114,   101,     3,    64,    65,    13,    99,
      15,   108,   102,     6,     6,    25,   319,    10,   321,   322,
      30,     3,     6,    33,   461,   342,    36,    37,   118,    13,
      10,     3,   288,   106,    44,    45,   109,    47,    47,    48,
     343,    23,   298,   109,    79,    41,    28,    29,    30,   115,
      13,    23,    57,    58,    59,    60,    28,    29,    30,    55,
      56,     6,   444,     3,    99,    10,    48,   102,    78,   311,
       6,   109,    31,    32,    10,   113,    48,    99,   351,   352,
     102,     3,   445,   446,    94,     1,    13,     3,    98,     5,
       6,   104,     8,     9,    10,     3,     4,   370,    72,    73,
     104,    17,    18,    93,    20,    21,    22,    23,    24,   116,
      26,    45,    28,    29,    30,   100,   101,    33,     3,    35,
      52,    53,    50,    39,    40,    41,   111,    43,    44,   114,
      46,     3,    48,    49,    50,    51,    96,    97,   468,   469,
       3,    15,   415,    13,    15,    61,    62,    37,    55,   405,
      66,    67,    68,    69,    70,    15,   412,    15,    64,     6,
     416,    16,    37,     5,     7,    81,   103,    83,    84,    28,
      86,    38,    31,    89,    90,    91,    35,    93,    94,    99,
      39,    40,     6,    42,    43,     6,    13,     6,   112,     3,
      21,    22,   117,    24,     3,    13,   112,     3,    13,   472,
     116,   117,    33,    41,    35,    82,    13,    15,    39,    40,
      41,    15,    43,    44,    15,    46,    52,   473,   112,    50,
      51,     6,    95,   479,   480,    13,   112,    13,     3,     6,
      61,    62,    13,    74,    37,    66,    67,    68,    69,    70,
      13,    71,    72,    73,    13,   110,    76,    77,    78,    13,
      73,   107,    15,     3,    13,   112,     3,     3,     6,    90,
      91,    13,   113,    94,   118,    25,     6,   113,    13,   113,
      13,    10,   106,   102,   113,   105,   106,   107,   103,    13,
      38,     3,     3,    13,    13,    13,     6,   115,    54,   115,
       6,   113,     3,   113,    78,    95,    15,   107,   115,   250,
     453,    28,   365,   156,   117,   438,   259,   226,   402,   239,
     162,   467,   425,   313,   330,   236,   424,   451,   430
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,   104,   122,   123,     0,     1,    81,    89,   124,   125,
     127,   128,   134,    13,     3,    23,    28,    29,    30,    48,
     129,   130,   131,   132,   144,   145,   146,   222,    21,    22,
      24,    33,    35,    39,    40,    41,    43,    44,    46,    50,
      51,    61,    62,    66,    67,    68,    69,    70,    90,    91,
      94,   133,   135,   136,   141,   148,   150,   151,   153,   158,
     159,   160,   162,   169,   175,   177,   179,   180,   181,   182,
     242,   246,   247,   248,   250,    26,   104,   126,   105,     3,
     221,   222,   104,    25,   218,    20,    49,   185,   186,   187,
     188,   190,   191,   238,   222,   144,   221,    10,   149,   222,
     221,     3,    10,    36,    93,   101,   108,   142,   143,     3,
     222,     6,   221,   222,   222,   251,   221,   221,    13,   221,
     221,    10,   222,   222,     3,   222,     3,    11,    12,   100,
      13,   104,    93,   104,   130,    45,    31,    32,   219,   116,
     202,   203,   239,     6,    10,    83,    84,    86,   112,   117,
     205,   206,   207,   234,   235,   237,    50,   210,     8,     9,
      17,    18,   112,   194,   195,   196,   197,   199,   200,   202,
     222,    75,   106,   137,   138,    95,   137,   254,    87,    88,
     105,   107,   152,   222,   137,     3,     3,     3,   143,     3,
       6,    13,    57,    58,    59,    60,   157,    15,    13,    15,
     257,    55,    94,   170,   171,   171,    37,    13,    64,    65,
     178,    15,   252,   137,   254,    64,   184,   163,   164,     6,
       3,   107,    16,    37,   240,     5,   204,    38,   213,     7,
     208,     3,     4,    15,   103,   112,   120,   223,   224,   227,
     230,   233,    47,    48,   223,   103,   189,   170,    41,   119,
     214,    71,    78,   105,   106,   201,   225,   226,   194,    99,
     192,   196,   100,   101,   111,   114,   198,     6,     6,    13,
       6,    10,     6,    10,     6,    10,     6,   112,     3,    15,
      19,    98,    13,   117,     3,     3,    13,   216,   214,    13,
      63,    82,    15,    15,    13,    15,    52,   183,   165,   166,
     167,   257,   254,   243,   112,     6,   147,    13,   215,   220,
     114,   241,   205,   219,   112,   223,   223,     3,   233,    79,
      85,    99,   102,    76,    77,    78,   105,   106,   107,   225,
     229,   234,   234,   118,   214,    13,     3,   189,     6,    72,
      73,   113,   195,    74,   209,    13,    37,   255,    13,   154,
      13,    55,    56,   214,   208,   137,   110,   176,    73,    13,
     255,    15,   107,   139,   140,   109,   137,     3,   168,    82,
     256,    13,   112,   244,     6,    13,   206,     3,    13,   109,
     202,   220,     6,    10,    13,    14,    80,   227,   228,   231,
     232,   113,   223,   223,   223,     6,   228,   234,   234,   211,
     208,   223,   193,   109,   115,    13,   113,   206,   118,   257,
     257,    25,   161,    57,   172,   253,    13,     6,   166,   208,
     257,   255,   245,   113,   113,   109,   115,    13,   208,    10,
     109,   113,   208,     3,    13,   236,   113,   103,   212,   106,
     213,    13,   217,   137,    38,     3,     3,   137,    52,    53,
     173,   257,   137,    13,     3,   113,     6,   249,   249,   217,
     115,   113,   232,    13,   113,   190,   115,   219,   214,   214,
      54,   174,   256,   165,     6,   115,   208,    13,   215,   161,
     161,   257,   137,     3,   137,   137,    95,   155,    15,    96,
      97,   156
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int16 yyr1[] =
{
       0,   121,   123,   122,   122,   122,   125,   124,   126,   124,
     124,   127,   124,   128,   124,   124,   129,   129,   130,   131,
     131,   132,   131,   133,   133,   133,   133,   133,   133,   133,
     133,   133,   133,   133,   133,   133,   133,   133,   133,   133,
     133,   133,   133,   133,   133,   133,   134,   135,   135,   135,
     136,   137,   137,   138,   138,   139,   139,   140,   141,   141,
     141,   141,   142,   142,   143,   143,   144,   144,   145,   145,
     145,   145,   147,   146,   148,   149,   149,   150,   151,   151,
     151,   151,   152,   152,   152,   152,   153,   153,   153,   153,
     153,   154,   153,   155,   155,   156,   156,   156,   157,   157,
     157,   157,   157,   157,   157,   157,   158,   159,   159,   160,
     160,   160,   161,   161,   163,   162,   164,   162,   165,   165,
     166,   167,   167,   168,   168,   169,   169,   169,   170,   170,
     171,   172,   172,   172,   173,   173,   173,   174,   174,   175,
     175,   175,   175,   176,   176,   177,   177,   177,   178,   178,
     179,   180,   180,   181,   181,   182,   183,   183,   184,   184,
     184,   185,   185,   186,   186,   186,   187,   188,   189,   189,
     191,   192,   193,   190,   194,   194,   195,   195,   196,   196,
     196,   196,   196,   197,   197,   197,   197,   198,   198,   199,
     199,   200,   200,   200,   201,   201,   201,   201,   202,   203,
     203,   204,   204,   205,   205,   206,   206,   206,   206,   207,
     207,   208,   208,   209,   209,   211,   210,   210,   212,   212,
     213,   213,   214,   214,   215,   215,   216,   216,   217,   217,
     218,   218,   219,   219,   219,   220,   220,   221,   221,   222,
     223,   223,   223,   223,   223,   223,   224,   224,   224,   225,
     225,   226,   226,   227,   227,   227,   227,   227,   227,   228,
     228,   228,   228,   228,   228,   229,   229,   229,   229,   229,
     229,   230,   231,   231,   232,   233,   234,   234,   234,   235,
     236,   236,   236,   237,   238,   239,   240,   240,   241,   241,
     241,   241,   243,   242,   242,   242,   242,   244,   244,   245,
     245,   246,   246,   246,   247,   247,   248,   248,   248,   249,
     249,   250,   250,   251,   252,   252,   253,   253,   254,   254,
     255,   255,   256,   256,   257,   257
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     1,     0,     0,     3,     0,     4,
       2,     0,     3,     0,     2,     1,     1,     3,     2,     1,
       1,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     1,     3,     7,     3,
       3,     1,     0,     2,     2,     1,     0,     2,     1,     2,
       2,     2,     2,     1,     2,     2,     3,     1,     1,     1,
       1,     1,     0,     6,     2,     2,     1,     2,     3,     4,
       4,     4,     2,     2,     2,     2,     3,     2,     1,     4,
       4,     0,    11,     3,     0,     1,     1,     0,     1,     1,
       1,     1,     1,     1,     1,     2,     2,     2,     1,    10,
      10,     7,     1,     0,     0,     5,     0,     9,     1,     3,
       2,     1,     3,     2,     0,     3,     4,     5,     1,     0,
       6,     2,     4,     0,     1,     1,     0,     1,     0,     5,
       5,     3,     4,     1,     0,     4,     4,     3,     1,     3,
       2,     3,     2,     3,     7,     5,     1,     0,     0,     2,
       3,     1,     3,     1,     1,     1,     4,     4,     1,     0,
       0,     0,     0,     6,     3,     1,     2,     1,     2,     2,
       2,     2,     1,     3,     1,     1,     1,     3,     5,     1,
       1,     2,     5,     1,     1,     1,     1,     0,     3,     1,
       0,     1,     0,     1,     1,     2,     1,     3,     1,     3,
       1,     1,     0,     2,     0,     0,     6,     0,     1,     0,
       3,     0,     2,     0,     1,     0,     1,     0,     1,     0,
       4,     0,     1,     1,     0,     2,     1,     1,     0,     1,
       3,     3,     3,     3,     2,     1,     3,     4,     1,     2,
       2,     1,     0,     1,     2,     1,     2,     1,     1,     1,
       2,     4,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     4,     1,     3,     1,     1,     1,     1,     1,     6,
       2,     1,     0,     5,     3,     2,     3,     0,     3,     5,
       4,     0,     0,     5,     2,     2,     2,     3,     0,     2,
       0,     7,     7,     4,     1,     0,     2,     3,     6,     2,
       1,     1,     2,     0,     1,     0,     1,     0,     1,     0,
       1,     0,     1,     0,     1,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 360 "parser.y"
                                       { prepare_parse(); }
#line 2006 "parser.tab.c"
    break;

  case 3:
#line 361 "parser.y"
                                       { if (generate_code)
                                           addHistoryLine();
                                         resetQueryBuffer();
                                         YYACCEPT; }
#line 2015 "parser.tab.c"
    break;

  case 4:
#line 365 "parser.y"
                                      { YYACCEPT; }
#line 2021 "parser.tab.c"
    break;

  case 5:
#line 366 "parser.y"
                                      { YYACCEPT; }
#line 2027 "parser.tab.c"
    break;

  case 6:
#line 369 "parser.y"
                                         { prepare_input(); }
#line 2033 "parser.tab.c"
    break;

  case 7:
#line 370 "parser.y"
                                         { after_CorpusCommand((yyvsp[-1].cl)); }
#line 2039 "parser.tab.c"
    break;

  case 8:
#line 372 "parser.y"
                    {
                      if ((yyvsp[0].ival) == query_lock) {
                        query_lock = 0;
                      }
                      else {
                        fprintf(stdout, "ALERT! Query lock violation.\n");
                        printf("\n"); /* so CQP.pm won't block -- should no longer be needed after switching to .EOL. mechanism */
                        exit(1);
                      }
                    }
#line 2054 "parser.tab.c"
    break;

  case 11:
#line 383 "parser.y"
                                         {if (query_lock) {warn_query_lock_violation(); YYABORT;} }
#line 2060 "parser.tab.c"
    break;

  case 12:
#line 384 "parser.y"
                                         { }
#line 2066 "parser.tab.c"
    break;

  case 13:
#line 385 "parser.y"
                                         {if (query_lock) {warn_query_lock_violation(); YYABORT;} }
#line 2072 "parser.tab.c"
    break;

  case 14:
#line 386 "parser.y"
                                         { exit_cqp++; }
#line 2078 "parser.tab.c"
    break;

  case 15:
#line 387 "parser.y"
                                         { /* in case of syntax errors, don't save
                                              history file */
                                           synchronize();
                                           resetQueryBuffer();
                                           YYABORT; /* Oli did this:   yyerrok; */
                                           /* but we don't want to continue processing a line, when part of it failed */
                                         }
#line 2090 "parser.tab.c"
    break;

  case 16:
#line 396 "parser.y"
                                        { (yyval.cl) = (yyvsp[0].cl); }
#line 2096 "parser.tab.c"
    break;

  case 17:
#line 398 "parser.y"
                                        { (yyval.cl) = in_CorpusCommand((yyvsp[-2].strval), (yyvsp[0].cl)); }
#line 2102 "parser.tab.c"
    break;

  case 18:
#line 402 "parser.y"
                                        { (yyval.cl) = in_UnnamedCorpusCommand((yyvsp[-1].cl)); }
#line 2108 "parser.tab.c"
    break;

  case 19:
#line 405 "parser.y"
                                        { if (query_lock) {warn_query_lock_violation(); YYABORT;} (yyval.cl) = ActivateCorpus((yyvsp[0].cl)); }
#line 2114 "parser.tab.c"
    break;

  case 20:
#line 406 "parser.y"
                                        { (yyval.cl) = after_CorpusSetExpr((yyvsp[0].cl)); }
#line 2120 "parser.tab.c"
    break;

  case 21:
#line 407 "parser.y"
                                        { prepare_Query(); }
#line 2126 "parser.tab.c"
    break;

  case 22:
#line 408 "parser.y"
                                        { (yyval.cl) = after_Query((yyvsp[0].cl)); }
#line 2132 "parser.tab.c"
    break;

  case 46:
#line 438 "parser.y"
                                        { printf("-::-EOL-::-\n"); fflush(stdout); }
#line 2138 "parser.tab.c"
    break;

  case 47:
#line 442 "parser.y"
                                              { do_cat((yyvsp[-1].cl), &((yyvsp[0].redir)), 0, -1); }
#line 2144 "parser.tab.c"
    break;

  case 48:
#line 445 "parser.y"
                                              { do_cat((yyvsp[-5].cl), &((yyvsp[0].redir)), (yyvsp[-3].ival), (yyvsp[-1].ival)); }
#line 2150 "parser.tab.c"
    break;

  case 49:
#line 448 "parser.y"
                          { if (generate_code)
                              do_cat((yyvsp[-1].cl), &((yyvsp[0].redir)), 0, -1);
                            drop_temp_corpora();
                          }
#line 2159 "parser.tab.c"
    break;

  case 50:
#line 455 "parser.y"
                                         { do_save((yyvsp[-1].cl), &((yyvsp[0].redir))); cl_free((yyvsp[0].redir).name); }
#line 2165 "parser.tab.c"
    break;

  case 52:
#line 459 "parser.y"
                                        { (yyval.redir).name = (char *)NULL;
                                          (yyval.redir).mode = (char *)NULL;
                                          (yyval.redir).stream = stdout;
                                          (yyval.redir).is_pipe = 0;
                                        }
#line 2175 "parser.tab.c"
    break;

  case 53:
#line 466 "parser.y"
                                        { (yyval.redir).name = (yyvsp[0].strval);
                                          (yyval.redir).mode = "w";
                                          (yyval.redir).stream = NULL;
                                          (yyval.redir).is_pipe = 0;
                                        }
#line 2185 "parser.tab.c"
    break;

  case 54:
#line 471 "parser.y"
                                        { (yyval.redir).name = (yyvsp[0].strval);
                                          (yyval.redir).mode = "a";
                                          (yyval.redir).stream = NULL;
                                          (yyval.redir).is_pipe = 0;
                                        }
#line 2195 "parser.tab.c"
    break;

  case 56:
#line 480 "parser.y"
                                        { (yyval.in_redir).name = (char *)NULL;
                                          (yyval.in_redir).stream = stdin;
                                          (yyval.in_redir).is_pipe = 0;
                                        }
#line 2204 "parser.tab.c"
    break;

  case 57:
#line 486 "parser.y"
                                        { (yyval.in_redir).name = (yyvsp[0].strval);
                                          (yyval.in_redir).stream = NULL;
                                          (yyval.in_redir).is_pipe = 0;
                                        }
#line 2213 "parser.tab.c"
    break;

  case 58:
#line 492 "parser.y"
                                        {
                                          show_corpora_files(UNDEF);
                                        }
#line 2221 "parser.tab.c"
    break;

  case 59:
#line 495 "parser.y"
                                        {
                                          if (strncasecmp((yyvsp[0].strval), "var", 3) == 0) {
                                            do_PrintAllVariables();
                                          }
                                          else if ((strncasecmp((yyvsp[0].strval), "sys", 3) == 0) || (strncasecmp((yyvsp[0].strval), "corp", 4) == 0)) {
                                            show_corpora_files(SYSTEM);
                                          }
                                          else if ((strncasecmp((yyvsp[0].strval), "sub", 3) == 0) || (strcasecmp((yyvsp[0].strval), "named") == 0) || (strcasecmp((yyvsp[0].strval), "queries") == 0)) {
                                            show_corpora_files(SUB);
                                          }
                                          else {
                                            cqpmessage(Error, "show what?");
                                          }
                                        }
#line 2240 "parser.tab.c"
    break;

  case 61:
#line 512 "parser.y"
                                        { PrintContextDescriptor(&CD); }
#line 2246 "parser.tab.c"
    break;

  case 64:
#line 522 "parser.y"
                                                { do_attribute_show((yyvsp[0].strval), 1); }
#line 2252 "parser.tab.c"
    break;

  case 65:
#line 523 "parser.y"
                                                { do_attribute_show((yyvsp[0].strval), 0); }
#line 2258 "parser.tab.c"
    break;

  case 66:
#line 526 "parser.y"
                                        { if (query_lock) {warn_query_lock_violation(); YYABORT;} (yyval.cl) = do_setop((yyvsp[-2].rngsetop), (yyvsp[-1].cl), (yyvsp[0].cl)); }
#line 2264 "parser.tab.c"
    break;

  case 68:
#line 530 "parser.y"
                                        { (yyval.rngsetop) = RUnion; }
#line 2270 "parser.tab.c"
    break;

  case 69:
#line 531 "parser.y"
                                        { (yyval.rngsetop) = RUnion; }
#line 2276 "parser.tab.c"
    break;

  case 70:
#line 532 "parser.y"
                                        { (yyval.rngsetop) = RIntersection; }
#line 2282 "parser.tab.c"
    break;

  case 71:
#line 533 "parser.y"
                                        { (yyval.rngsetop) = RDiff; }
#line 2288 "parser.tab.c"
    break;

  case 72:
#line 539 "parser.y"
                                        {
                                          do_start_timer();
                                          prepare_do_subset((yyvsp[-2].cl), (yyvsp[0].field));
                                          next_environment();   /* create environment for pattern compilation (will be freed in prepare_input() before next command) */
                                        }
#line 2298 "parser.tab.c"
    break;

  case 73:
#line 544 "parser.y"
                                        {
                                          if (generate_code) {
                                            (yyval.cl) = do_subset((yyvsp[-2].field), (yyvsp[0].boolt));
                                            do_timing("Subset computed");
                                          }
                                          else
                                            (yyval.cl) = NULL;
                                        }
#line 2311 "parser.tab.c"
    break;

  case 74:
#line 555 "parser.y"
                                         {  }
#line 2317 "parser.tab.c"
    break;

  case 75:
#line 558 "parser.y"
                                       { if ((yyvsp[0].cl))
                                            dropcorpus((yyvsp[0].cl));
                                        }
#line 2325 "parser.tab.c"
    break;

  case 76:
#line 561 "parser.y"
                                        { if ((yyvsp[0].cl))
                                            dropcorpus((yyvsp[0].cl));
                                        }
#line 2333 "parser.tab.c"
    break;

  case 77:
#line 566 "parser.y"
                                  { do_PrintVariableValue((yyvsp[0].strval));
                                    free((yyvsp[0].strval));
                                  }
#line 2341 "parser.tab.c"
    break;

  case 78:
#line 573 "parser.y"
                                  { do_SetVariableValue((yyvsp[-1].strval),
                                                        (yyvsp[0].varsetting).operator,
                                                        (yyvsp[0].varsetting).variableValue);
                                    free((yyvsp[-1].strval));
                                    free((yyvsp[0].varsetting).variableValue);
                                  }
#line 2352 "parser.tab.c"
    break;

  case 79:
#line 580 "parser.y"
                                                    {
                                    do_AddSubVariables((yyvsp[-2].strval), /*add*/1, (yyvsp[0].strval));
                                    free((yyvsp[-2].strval));
                                    free((yyvsp[0].strval));
                                  }
#line 2362 "parser.tab.c"
    break;

  case 80:
#line 586 "parser.y"
                                                     {
                                    do_AddSubVariables((yyvsp[-2].strval), /*sub*/0, (yyvsp[0].strval));
                                    free((yyvsp[-2].strval));
                                    free((yyvsp[0].strval));
                                  }
#line 2372 "parser.tab.c"
    break;

  case 81:
#line 592 "parser.y"
                                                 {
                                    char *temp = cl_strdup("");
                                    do_SetVariableValue((yyvsp[-2].strval), '=', temp);         /* cheap trick, this is :o) */
                                    free(temp);
                                    do_AddSubVariables((yyvsp[-2].strval), /*add*/1, (yyvsp[0].strval));
                                    free((yyvsp[-2].strval));
                                    free((yyvsp[0].strval));
                                  }
#line 2385 "parser.tab.c"
    break;

  case 82:
#line 603 "parser.y"
                             { (yyval.varsetting).variableValue = (yyvsp[0].strval); (yyval.varsetting).operator = '<'; }
#line 2391 "parser.tab.c"
    break;

  case 83:
#line 604 "parser.y"
                             { (yyval.varsetting).variableValue = (yyvsp[0].strval); (yyval.varsetting).operator = '='; }
#line 2397 "parser.tab.c"
    break;

  case 84:
#line 605 "parser.y"
                                { (yyval.varsetting).variableValue = (yyvsp[0].strval); (yyval.varsetting).operator = '+'; }
#line 2403 "parser.tab.c"
    break;

  case 85:
#line 606 "parser.y"
                                 { (yyval.varsetting).variableValue = (yyvsp[0].strval); (yyval.varsetting).operator = '-'; }
#line 2409 "parser.tab.c"
    break;

  case 86:
#line 609 "parser.y"
                                        { char *msg;

                                          if ((yyvsp[0].varval).cval != NULL && (yyvsp[0].varval).ival >= 0) {
                                            msg = set_context_option_value((yyvsp[-1].strval), (yyvsp[0].varval).cval, (yyvsp[0].varval).ival);
                                          }
                                          else if ((yyvsp[0].varval).cval != NULL) {
                                            /* get rid of quotes at start and end of value */
                                            /* -- removed because quotes should be stripped by lexer ({string} rule in parser.l) */
                                            /*
                                            if (($3.cval[0] == '"') && ($3.cval[strlen($3.cval)-1] == '"')
                                                || ($3.cval[0] == '\'') && ($3.cval[strlen($3.cval)-1] == '\'') ) {


                                              $3.cval[strlen($3.cval)-1] = '\0';
                                              $3.cval = $3.cval + 1;
                                            }
                                            */
                                            msg = set_string_option_value((yyvsp[-1].strval), (yyvsp[0].varval).cval);
                                          }
                                          else
                                            msg = set_integer_option_value((yyvsp[-1].strval), (yyvsp[0].varval).ival);

                                          if (msg != NULL)
                                            cqpmessage(Warning,
                                                       "Option set error:\n%s", msg);
                                        }
#line 2440 "parser.tab.c"
    break;

  case 87:
#line 635 "parser.y"
                                        { int opt;

                                          if ((opt = find_option((yyvsp[0].strval))) >= 0)
                                            print_option_value(opt);
                                          else
                                            cqpmessage(Warning,
                                                     "Unknown option: ``%s''\n", (yyvsp[0].strval));
                                        }
#line 2453 "parser.tab.c"
    break;

  case 88:
#line 643 "parser.y"
                                        {
                                          print_option_values();
                                        }
#line 2461 "parser.tab.c"
    break;

  case 89:
#line 646 "parser.y"
                                        { do_set_target((yyvsp[-2].cl), (yyvsp[-1].field), (yyvsp[0].field)); }
#line 2467 "parser.tab.c"
    break;

  case 90:
#line 647 "parser.y"
                                           { do_set_target((yyvsp[-2].cl), (yyvsp[-1].field), NoField); }
#line 2473 "parser.tab.c"
    break;

  case 91:
#line 648 "parser.y"
                                                  {
                                          if (generate_code) {
                                            old_query_corpus = query_corpus;
                                            query_corpus = (yyvsp[-2].cl);  /* set query_corpus for compiling the ExtConstraint pattern */
                                            next_environment(); /* create environment for pattern compilation (will be freed in prepare_input() before next command) */
                                            do_start_timer();
                                          }
                                        }
#line 2486 "parser.tab.c"
    break;

  case 92:
#line 658 "parser.y"
                                        {
                                          do_set_complex_target((yyvsp[-9].cl), (yyvsp[-8].field), (yyvsp[-7].search_strategy), (yyvsp[-5].boolt), (yyvsp[-3].direction), (yyvsp[-2].ival), (yyvsp[-1].strval), (yyvsp[0].base).field, (yyvsp[0].base).inclusive);
                                          if (generate_code)
                                            do_timing("``set target ...'' completed");
                                        }
#line 2496 "parser.tab.c"
    break;

  case 93:
#line 665 "parser.y"
                                                   {
                                          /* from (match|keyword|target) [inclusive|exclusive] */
                                          (yyval.base).field = (yyvsp[-1].field);
                                          (yyval.base).inclusive = (yyvsp[0].ival);
                                        }
#line 2506 "parser.tab.c"
    break;

  case 94:
#line 670 "parser.y"
                                        {
                                          (yyval.base).field = MatchField;
                                          (yyval.base).inclusive = 0;
                                        }
#line 2515 "parser.tab.c"
    break;

  case 95:
#line 676 "parser.y"
                                        { (yyval.ival) = 1; }
#line 2521 "parser.tab.c"
    break;

  case 96:
#line 677 "parser.y"
                                        { (yyval.ival) = 0; }
#line 2527 "parser.tab.c"
    break;

  case 97:
#line 678 "parser.y"
                                        { (yyval.ival) = 0; /* default is exclusive */ }
#line 2533 "parser.tab.c"
    break;

  case 98:
#line 681 "parser.y"
                                        { (yyval.varval).ival = (yyvsp[0].ival);
                                          (yyval.varval).cval = NULL;
                                          (yyval.varval).ok = 1;
                                        }
#line 2542 "parser.tab.c"
    break;

  case 99:
#line 685 "parser.y"
                                        { (yyval.varval).ival = -1;
                                          (yyval.varval).cval = (yyvsp[0].strval);
                                          (yyval.varval).ok = 1;
                                        }
#line 2551 "parser.tab.c"
    break;

  case 100:
#line 689 "parser.y"
                                        {
                                          (yyval.varval).ival = -1;
                                          (yyval.varval).cval = (yyvsp[0].strval);
                                          (yyval.varval).ok = 1;
                                        }
#line 2561 "parser.tab.c"
    break;

  case 101:
#line 694 "parser.y"
                                        { (yyval.varval).ival = 1; (yyval.varval).cval = NULL; (yyval.varval).ok = 1; }
#line 2567 "parser.tab.c"
    break;

  case 102:
#line 695 "parser.y"
                                        { (yyval.varval).ival = 1; (yyval.varval).cval = NULL; (yyval.varval).ok = 1; }
#line 2573 "parser.tab.c"
    break;

  case 103:
#line 696 "parser.y"
                                        { (yyval.varval).ival = 0; (yyval.varval).cval = NULL; (yyval.varval).ok = 1; }
#line 2579 "parser.tab.c"
    break;

  case 104:
#line 697 "parser.y"
                                        { (yyval.varval).ival = 0; (yyval.varval).cval = NULL; (yyval.varval).ok = 1; }
#line 2585 "parser.tab.c"
    break;

  case 105:
#line 698 "parser.y"
                                        {
                                          (yyval.varval).ival = (yyvsp[-1].ival);
                                          (yyval.varval).cval = (yyvsp[0].strval);
                                          (yyval.varval).ok = 1;
                                        }
#line 2595 "parser.tab.c"
    break;

  case 106:
#line 705 "parser.y"
                                        { do_exec((yyvsp[0].strval)); }
#line 2601 "parser.tab.c"
    break;

  case 107:
#line 708 "parser.y"
                                        { do_info((yyvsp[0].cl)); }
#line 2607 "parser.tab.c"
    break;

  case 108:
#line 709 "parser.y"
                                        { do_info(current_corpus); }
#line 2613 "parser.tab.c"
    break;

  case 109:
#line 714 "parser.y"
                                {
                                  do_group((yyvsp[-8].cl), (yyvsp[-7].Anchor).anchor, (yyvsp[-7].Anchor).offset, (yyvsp[-6].strval), (yyvsp[-4].Anchor).anchor, (yyvsp[-4].Anchor).offset, (yyvsp[-3].strval), (yyvsp[-2].ival), (yyvsp[-1].ival), &((yyvsp[0].redir)));
                                  cl_free((yyvsp[0].redir).name);
                                }
#line 2622 "parser.tab.c"
    break;

  case 110:
#line 720 "parser.y"
                                {
                                  do_group((yyvsp[-8].cl), (yyvsp[-7].Anchor).anchor, (yyvsp[-7].Anchor).offset, (yyvsp[-6].strval), (yyvsp[-4].Anchor).anchor, (yyvsp[-4].Anchor).offset, (yyvsp[-3].strval), (yyvsp[-2].ival), (yyvsp[-1].ival), &((yyvsp[0].redir)));
                                  cl_free((yyvsp[0].redir).name);
                                }
#line 2631 "parser.tab.c"
    break;

  case 111:
#line 726 "parser.y"
                                {
                                  do_group2((yyvsp[-5].cl), (yyvsp[-4].Anchor).anchor, (yyvsp[-4].Anchor).offset, (yyvsp[-3].strval), (yyvsp[-2].ival), (yyvsp[-1].ival), &((yyvsp[0].redir)));
                                  cl_free((yyvsp[0].redir).name);
                                }
#line 2640 "parser.tab.c"
    break;

  case 112:
#line 733 "parser.y"
                            { (yyval.ival) = 1; }
#line 2646 "parser.tab.c"
    break;

  case 113:
#line 734 "parser.y"
                               { (yyval.ival) = 0; }
#line 2652 "parser.tab.c"
    break;

  case 114:
#line 740 "parser.y"
                   { free_tabulation_list(); }
#line 2658 "parser.tab.c"
    break;

  case 115:
#line 742 "parser.y"
                   { print_tabulation((yyvsp[-3].cl), 0, INT_MAX, &((yyvsp[0].redir))); cl_free((yyvsp[0].redir).name); }
#line 2664 "parser.tab.c"
    break;

  case 116:
#line 744 "parser.y"
                   { free_tabulation_list(); }
#line 2670 "parser.tab.c"
    break;

  case 117:
#line 746 "parser.y"
                   { print_tabulation((yyvsp[-7].cl), (yyvsp[-4].ival), (yyvsp[-2].ival), &((yyvsp[0].redir))); cl_free((yyvsp[0].redir).name); }
#line 2676 "parser.tab.c"
    break;

  case 118:
#line 750 "parser.y"
                   { append_tabulation_item((yyvsp[0].tabulation_item)); }
#line 2682 "parser.tab.c"
    break;

  case 119:
#line 752 "parser.y"
                   { append_tabulation_item((yyvsp[0].tabulation_item)); }
#line 2688 "parser.tab.c"
    break;

  case 120:
#line 756 "parser.y"
                   {
                     (yyval.tabulation_item) = new_tabulation_item();
                     (yyval.tabulation_item)->attribute_name = (yyvsp[0].AttributeSpecification).name;
                     (yyval.tabulation_item)->flags   = (yyvsp[0].AttributeSpecification).flags;
                     (yyval.tabulation_item)->anchor1 = (yyvsp[-1].AnchorPair).anchor1;
                     (yyval.tabulation_item)->offset1 = (yyvsp[-1].AnchorPair).offset1;
                     (yyval.tabulation_item)->anchor2 = (yyvsp[-1].AnchorPair).anchor2;
                     (yyval.tabulation_item)->offset2 = (yyvsp[-1].AnchorPair).offset2;
                   }
#line 2702 "parser.tab.c"
    break;

  case 121:
#line 768 "parser.y"
                     { (yyval.AnchorPair).anchor1 = (yyval.AnchorPair).anchor2 = (yyvsp[0].Anchor).anchor; (yyval.AnchorPair).offset1 = (yyval.AnchorPair).offset2 = (yyvsp[0].Anchor).offset; }
#line 2708 "parser.tab.c"
    break;

  case 122:
#line 770 "parser.y"
                     { (yyval.AnchorPair).anchor1 = (yyvsp[-2].Anchor).anchor; (yyval.AnchorPair).offset1 = (yyvsp[-2].Anchor).offset;
                       (yyval.AnchorPair).anchor2 = (yyvsp[0].Anchor).anchor; (yyval.AnchorPair).offset2 = (yyvsp[0].Anchor).offset; }
#line 2715 "parser.tab.c"
    break;

  case 123:
#line 775 "parser.y"
                       { (yyval.AttributeSpecification).name = (yyvsp[-1].strval); (yyval.AttributeSpecification).flags = (yyvsp[0].ival); }
#line 2721 "parser.tab.c"
    break;

  case 124:
#line 777 "parser.y"
                       { (yyval.AttributeSpecification).name = NULL; (yyval.AttributeSpecification).flags = 0; }
#line 2727 "parser.tab.c"
    break;

  case 125:
#line 787 "parser.y"
                {
                  int ok;
                  if ((yyvsp[-1].cl) && generate_code) {
                    do_start_timer();
                    ok = SortSubcorpus((yyvsp[-1].cl), (yyvsp[0].sortclause), 0, NULL);
                    FreeSortClause((yyvsp[0].sortclause));
                    do_timing("Query result sorted");
                    if (autoshow && ok && ((yyvsp[-1].cl)->size > 0))
                      catalog_corpus((yyvsp[-1].cl), NULL, 0, -1, GlobalPrintMode);
                  }
                }
#line 2743 "parser.tab.c"
    break;

  case 126:
#line 799 "parser.y"
                {
                  int ok;
                  if ((yyvsp[-2].cl) && generate_code) {
                    do_start_timer();
                    ok = SortSubcorpusRandomize((yyvsp[-2].cl), (yyvsp[0].ival));
                    do_timing("Query result randomized");
                    if (autoshow && ok && ((yyvsp[-2].cl)->size > 0))
                      catalog_corpus((yyvsp[-2].cl), NULL, 0, -1, GlobalPrintMode);
                  }
                }
#line 2758 "parser.tab.c"
    break;

  case 127:
#line 810 "parser.y"
                {
                  int ok;
                  if ((yyvsp[-3].cl) && generate_code) {
                    ok = SortSubcorpus((yyvsp[-3].cl), (yyvsp[-2].sortclause), ((yyvsp[-1].ival) >= 1) ? (yyvsp[-1].ival) : 1, &((yyvsp[0].redir)));
                    FreeSortClause((yyvsp[-2].sortclause));
                    cl_free((yyvsp[0].redir).name);
                  }
                }
#line 2771 "parser.tab.c"
    break;

  case 128:
#line 821 "parser.y"
                                        { (yyval.sortclause) = (yyvsp[0].sortclause); }
#line 2777 "parser.tab.c"
    break;

  case 129:
#line 822 "parser.y"
                                        { (yyval.sortclause) = NULL; }
#line 2783 "parser.tab.c"
    break;

  case 130:
#line 826 "parser.y"
                {
                  if (generate_code) {
                    (yyval.sortclause) = cl_malloc(sizeof(SortClauseBuffer));
                    (yyval.sortclause)->attribute_name  = (yyvsp[-4].strval);
                    (yyval.sortclause)->flags           = (yyvsp[-3].ival);
                    (yyval.sortclause)->anchor1         = (yyvsp[-2].AnchorPair).anchor1;
                    (yyval.sortclause)->offset1         = (yyvsp[-2].AnchorPair).offset1;
                    (yyval.sortclause)->anchor2         = (yyvsp[-2].AnchorPair).anchor2;
                    (yyval.sortclause)->offset2         = (yyvsp[-2].AnchorPair).offset2;
                    (yyval.sortclause)->sort_ascending  = (yyvsp[-1].ival);
                    (yyval.sortclause)->sort_reverse    = (yyvsp[0].ival);
                  }
                  else
                    (yyval.sortclause) = NULL;
                }
#line 2803 "parser.tab.c"
    break;

  case 131:
#line 843 "parser.y"
                             { (yyval.AnchorPair).anchor1 = (yyval.AnchorPair).anchor2 = (yyvsp[0].Anchor).anchor; (yyval.AnchorPair).offset1 = (yyval.AnchorPair).offset2 = (yyvsp[0].Anchor).offset; }
#line 2809 "parser.tab.c"
    break;

  case 132:
#line 845 "parser.y"
                            { (yyval.AnchorPair).anchor1 = (yyvsp[-2].Anchor).anchor; (yyval.AnchorPair).offset1 = (yyvsp[-2].Anchor).offset;
                              (yyval.AnchorPair).anchor2 = (yyvsp[0].Anchor).anchor; (yyval.AnchorPair).offset2 = (yyvsp[0].Anchor).offset; }
#line 2816 "parser.tab.c"
    break;

  case 133:
#line 848 "parser.y"
                            { (yyval.AnchorPair).anchor1 = MatchField;    (yyval.AnchorPair).offset1 = 0;
                              (yyval.AnchorPair).anchor2 = MatchEndField; (yyval.AnchorPair).offset2 = 0; }
#line 2823 "parser.tab.c"
    break;

  case 134:
#line 852 "parser.y"
                              { (yyval.ival) = 1; }
#line 2829 "parser.tab.c"
    break;

  case 135:
#line 853 "parser.y"
                              { (yyval.ival) = 0; }
#line 2835 "parser.tab.c"
    break;

  case 136:
#line 854 "parser.y"
                              { (yyval.ival) = 1; }
#line 2841 "parser.tab.c"
    break;

  case 137:
#line 857 "parser.y"
                              { (yyval.ival) = 1; }
#line 2847 "parser.tab.c"
    break;

  case 138:
#line 858 "parser.y"
                              { (yyval.ival) = 0; }
#line 2853 "parser.tab.c"
    break;

  case 139:
#line 864 "parser.y"
                                        {
                                          do_reduce((yyvsp[-3].cl), (yyvsp[-1].ival), (yyvsp[0].ival));
                                        }
#line 2861 "parser.tab.c"
    break;

  case 140:
#line 868 "parser.y"
                                        {
                                          RangeSetop((yyvsp[-3].cl), RMaximalMatches, NULL, NULL);
                                        }
#line 2869 "parser.tab.c"
    break;

  case 141:
#line 872 "parser.y"
                                        {
                                          do_cut((yyvsp[-1].cl), 0, (yyvsp[0].ival)-1);
                                        }
#line 2877 "parser.tab.c"
    break;

  case 142:
#line 876 "parser.y"
                                        {
                                          do_cut((yyvsp[-2].cl), (yyvsp[-1].ival), (yyvsp[0].ival));
                                        }
#line 2885 "parser.tab.c"
    break;

  case 143:
#line 881 "parser.y"
                                { (yyval.ival) = 1; }
#line 2891 "parser.tab.c"
    break;

  case 144:
#line 882 "parser.y"
                                { (yyval.ival) = 0; }
#line 2897 "parser.tab.c"
    break;

  case 145:
#line 886 "parser.y"
                                        {
                                          if ((yyvsp[-2].cl) && generate_code) {
                                            do_delete_lines((yyvsp[-2].cl), (yyvsp[0].field), SELECTED_LINES);
                                          }
                                        }
#line 2907 "parser.tab.c"
    break;

  case 146:
#line 892 "parser.y"
                                        {
                                          if ((yyvsp[-2].cl) && generate_code) {
                                            do_delete_lines((yyvsp[-2].cl), (yyvsp[0].field), UNSELECTED_LINES);
                                          }
                                        }
#line 2917 "parser.tab.c"
    break;

  case 147:
#line 898 "parser.y"
                                        {
                                          if ((yyvsp[-1].cl) && generate_code) {
                                             do_delete_lines_num((yyvsp[-1].cl), (yyvsp[0].Distance).mindist, (yyvsp[0].Distance).maxdist);
                                           }
                                         }
#line 2927 "parser.tab.c"
    break;

  case 148:
#line 905 "parser.y"
                                           { (yyval.Distance).mindist = (yyvsp[0].ival); (yyval.Distance).maxdist = (yyvsp[0].ival); }
#line 2933 "parser.tab.c"
    break;

  case 149:
#line 906 "parser.y"
                                           { (yyval.Distance).mindist = (yyvsp[-2].ival); (yyval.Distance).maxdist = (yyvsp[0].ival); }
#line 2939 "parser.tab.c"
    break;

  case 150:
#line 911 "parser.y"
                             { do_sleep((yyvsp[0].ival)); }
#line 2945 "parser.tab.c"
    break;

  case 151:
#line 915 "parser.y"
                                       { do_size((yyvsp[-1].cl), (yyvsp[0].field)); }
#line 2951 "parser.tab.c"
    break;

  case 152:
#line 916 "parser.y"
                              { do_printVariableSize((yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 2957 "parser.tab.c"
    break;

  case 153:
#line 921 "parser.y"
            { do_dump((yyvsp[-1].cl), 0, INT_MAX, &((yyvsp[0].redir))); cl_free((yyvsp[0].redir).name); }
#line 2963 "parser.tab.c"
    break;

  case 154:
#line 923 "parser.y"
            { do_dump((yyvsp[-5].cl), (yyvsp[-3].ival), (yyvsp[-1].ival), &((yyvsp[0].redir))); cl_free((yyvsp[0].redir).name); }
#line 2969 "parser.tab.c"
    break;

  case 155:
#line 927 "parser.y"
            { do_undump((yyvsp[-3].strval), (yyvsp[-2].ival), !(yyvsp[-1].ival), &((yyvsp[0].in_redir))); cl_free((yyvsp[0].in_redir).name); }
#line 2975 "parser.tab.c"
    break;

  case 156:
#line 930 "parser.y"
                              { (yyval.ival) = 1; }
#line 2981 "parser.tab.c"
    break;

  case 157:
#line 931 "parser.y"
                              { (yyval.ival) = 0; }
#line 2987 "parser.tab.c"
    break;

  case 158:
#line 935 "parser.y"
                              { (yyval.ival) = 0; }
#line 2993 "parser.tab.c"
    break;

  case 159:
#line 937 "parser.y"
          {
            if ((yyvsp[0].field) == TargetField) { (yyval.ival) = 1; }
            else { yyerror("Invalid extension anchor in undump command"); YYABORT; }
          }
#line 3002 "parser.tab.c"
    break;

  case 160:
#line 942 "parser.y"
          {
            if ( (((yyvsp[-1].field) == TargetField) && ((yyvsp[0].field) == KeywordField))
                 || (((yyvsp[0].field) == TargetField) && ((yyvsp[-1].field) == KeywordField))
               ) { (yyval.ival) = 2; }
            else { yyerror("Invalid extension anchor in undump command"); YYABORT; }
          }
#line 3013 "parser.tab.c"
    break;

  case 162:
#line 954 "parser.y"
                {
                  if ((yyvsp[-2].cl) && (yyvsp[0].sortclause) && (yyvsp[-2].cl)->size > 0) {
                    SortSubcorpus((yyvsp[-2].cl), (yyvsp[0].sortclause), 0, NULL);
                    FreeSortClause((yyvsp[0].sortclause));
                  }
                  (yyval.cl) = (yyvsp[-2].cl);
                }
#line 3025 "parser.tab.c"
    break;

  case 166:
#line 970 "parser.y"
                                        { (yyval.cl) = do_StandardQuery((yyvsp[-1].ival), (yyvsp[0].ival)); }
#line 3031 "parser.tab.c"
    break;

  case 167:
#line 974 "parser.y"
                                                   { (yyval.cl) = do_MUQuery((yyvsp[-2].evalt), (yyvsp[-1].ival), (yyvsp[0].ival)); }
#line 3037 "parser.tab.c"
    break;

  case 168:
#line 977 "parser.y"
                                        { (yyval.ival) = 1; }
#line 3043 "parser.tab.c"
    break;

  case 169:
#line 978 "parser.y"
                                        { (yyval.ival) = 0; }
#line 3049 "parser.tab.c"
    break;

  case 170:
#line 981 "parser.y"
                                        { if (generate_code) { CurEnv->match_label = labellookup(CurEnv->labels, "match", LAB_DEFINED, 1); } }
#line 3055 "parser.tab.c"
    break;

  case 171:
#line 982 "parser.y"
                                        { within_gc = 1;
                                          if (generate_code) { CurEnv->matchend_label = labellookup(CurEnv->labels, "matchend", LAB_DEFINED, 1); } }
#line 3062 "parser.tab.c"
    break;

  case 172:
#line 984 "parser.y"
                                        { within_gc = 0; }
#line 3068 "parser.tab.c"
    break;

  case 173:
#line 985 "parser.y"
                                        { do_SearchPattern((yyvsp[-4].evalt), (yyvsp[-2].boolt)); }
#line 3074 "parser.tab.c"
    break;

  case 174:
#line 989 "parser.y"
                                        { (yyval.evalt) = reg_disj((yyvsp[-2].evalt), (yyvsp[0].evalt)); }
#line 3080 "parser.tab.c"
    break;

  case 175:
#line 990 "parser.y"
                                        { (yyval.evalt) = (yyvsp[0].evalt); }
#line 3086 "parser.tab.c"
    break;

  case 176:
#line 994 "parser.y"
                                        { (yyval.evalt) = reg_seq((yyvsp[-1].evalt), (yyvsp[0].evalt)); }
#line 3092 "parser.tab.c"
    break;

  case 177:
#line 995 "parser.y"
                                        { (yyval.evalt) = (yyvsp[0].evalt); }
#line 3098 "parser.tab.c"
    break;

  case 178:
#line 998 "parser.y"
                                       { if (generate_code) {
                                           (yyval.evalt) = (yyvsp[0].evalt);
                                           (yyval.evalt)->node.left = (yyvsp[-1].evalt);
                                           (yyval.evalt)->node.right = NULL;
                                         }
                                         else
                                           (yyval.evalt) = NULL;
                                       }
#line 3111 "parser.tab.c"
    break;

  case 179:
#line 1006 "parser.y"
                                       { if (generate_code)
                                           NEW_EVALNODE((yyval.evalt), re_repeat, (yyvsp[-1].evalt), NULL, 0, repeat_inf);
                                         else
                                           (yyval.evalt) = NULL;
                                       }
#line 3121 "parser.tab.c"
    break;

  case 180:
#line 1011 "parser.y"
                                       { if (generate_code)
                                           NEW_EVALNODE((yyval.evalt), re_repeat, (yyvsp[-1].evalt), NULL, 1, repeat_inf);
                                         else
                                           (yyval.evalt) = NULL;
                                       }
#line 3131 "parser.tab.c"
    break;

  case 181:
#line 1016 "parser.y"
                                       { if (generate_code)
                                           NEW_EVALNODE((yyval.evalt), re_repeat, (yyvsp[-1].evalt), NULL, 0, 1);
                                         else
                                           (yyval.evalt) = NULL;
                                       }
#line 3141 "parser.tab.c"
    break;

  case 182:
#line 1021 "parser.y"
                                       { (yyval.evalt) = (yyvsp[0].evalt); }
#line 3147 "parser.tab.c"
    break;

  case 183:
#line 1024 "parser.y"
                                        { (yyval.evalt) = (yyvsp[-1].evalt); }
#line 3153 "parser.tab.c"
    break;

  case 184:
#line 1025 "parser.y"
                                        { if (generate_code)
                                            NEW_EVALLEAF((yyval.evalt), (yyvsp[0].index));
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3163 "parser.tab.c"
    break;

  case 185:
#line 1030 "parser.y"
                                        { if (generate_code)
                                            NEW_EVALLEAF((yyval.evalt), (yyvsp[0].index));
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3173 "parser.tab.c"
    break;

  case 186:
#line 1035 "parser.y"
                                        { if (generate_code)
                                            NEW_EVALLEAF((yyval.evalt), (yyvsp[0].index));
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3183 "parser.tab.c"
    break;

  case 187:
#line 1042 "parser.y"
                                        { if (generate_code)
                                            NEW_EVALNODE((yyval.evalt), re_repeat, NULL, NULL, (yyvsp[-1].ival), (yyvsp[-1].ival));
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3193 "parser.tab.c"
    break;

  case 188:
#line 1048 "parser.y"
                                        { if (generate_code)
                                            NEW_EVALNODE((yyval.evalt), re_repeat, NULL, NULL, (yyvsp[-3].ival), (yyvsp[-1].ival));
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3203 "parser.tab.c"
    break;

  case 189:
#line 1056 "parser.y"
                                        { (yyval.index) = do_AnchorPoint((yyvsp[0].field), 0); }
#line 3209 "parser.tab.c"
    break;

  case 190:
#line 1057 "parser.y"
                                        { (yyval.index) = do_AnchorPoint((yyvsp[0].field), 1); }
#line 3215 "parser.tab.c"
    break;

  case 191:
#line 1060 "parser.y"
                                        { (yyval.index) = do_XMLTag((yyvsp[-1].strval), 0, 0, NULL, 0); }
#line 3221 "parser.tab.c"
    break;

  case 192:
#line 1062 "parser.y"
                                        { (yyval.index) = do_XMLTag((yyvsp[-4].strval), 0, (yyvsp[-3].ival), (yyvsp[-2].strval), (yyvsp[-1].ival)); }
#line 3227 "parser.tab.c"
    break;

  case 193:
#line 1063 "parser.y"
                                        { (yyval.index) = do_XMLTag((yyvsp[0].strval), 1, 0, NULL, 0); }
#line 3233 "parser.tab.c"
    break;

  case 194:
#line 1066 "parser.y"
                                        { (yyval.ival) = (yyvsp[0].ival); }
#line 3239 "parser.tab.c"
    break;

  case 195:
#line 1067 "parser.y"
                                        { (yyval.ival) = OP_EQUAL | OP_NOT; }
#line 3245 "parser.tab.c"
    break;

  case 196:
#line 1068 "parser.y"
                                        { (yyval.ival) = OP_EQUAL; }
#line 3251 "parser.tab.c"
    break;

  case 197:
#line 1069 "parser.y"
                                        { (yyval.ival) = OP_EQUAL; }
#line 3257 "parser.tab.c"
    break;

  case 198:
#line 1074 "parser.y"
                                        { (yyval.index) = do_NamedWfPattern((yyvsp[-2].ival), (yyvsp[-1].strval), (yyvsp[0].index)); }
#line 3263 "parser.tab.c"
    break;

  case 199:
#line 1078 "parser.y"
                                        { (yyval.ival) = 1; }
#line 3269 "parser.tab.c"
    break;

  case 200:
#line 1079 "parser.y"
                                        { (yyval.ival) = 0; }
#line 3275 "parser.tab.c"
    break;

  case 201:
#line 1082 "parser.y"
                                        { (yyval.strval) = (yyvsp[0].strval); }
#line 3281 "parser.tab.c"
    break;

  case 202:
#line 1083 "parser.y"
                                        { (yyval.strval) = NULL; }
#line 3287 "parser.tab.c"
    break;

  case 203:
#line 1086 "parser.y"
                                        { (yyval.index) = do_WordformPattern((yyvsp[0].boolt), 0); }
#line 3293 "parser.tab.c"
    break;

  case 204:
#line 1087 "parser.y"
                                        { (yyval.index) = do_WordformPattern((yyvsp[0].boolt), 1); }
#line 3299 "parser.tab.c"
    break;

  case 205:
#line 1090 "parser.y"
                                      { (yyval.boolt) = do_StringConstraint((yyvsp[-1].strval), (yyvsp[0].ival)); }
#line 3305 "parser.tab.c"
    break;

  case 206:
#line 1091 "parser.y"
                                      { (yyval.boolt) = NULL;
                                        if (!FindVariable((yyvsp[0].strval))) {
                                          cqpmessage(Error,
                                                     "%s: no such variable",
                                                     (yyvsp[0].strval));
                                          generate_code = 0;
                                        }
                                        else {
                                          (yyval.boolt) = do_SimpleVariableReference((yyvsp[0].strval));
                                        }
                                        free((yyvsp[0].strval));
                                      }
#line 3322 "parser.tab.c"
    break;

  case 207:
#line 1103 "parser.y"
                                      { (yyval.boolt) = (yyvsp[-1].boolt); }
#line 3328 "parser.tab.c"
    break;

  case 208:
#line 1104 "parser.y"
                                      { if (generate_code) {
                                          NEW_BNODE((yyval.boolt));
                                          (yyval.boolt)->constnode.type = cnode;
                                          (yyval.boolt)->constnode.val  = 1;
                                        }
                                      }
#line 3339 "parser.tab.c"
    break;

  case 209:
#line 1112 "parser.y"
                                             { (yyval.boolt) = (yyvsp[-1].boolt); }
#line 3345 "parser.tab.c"
    break;

  case 210:
#line 1113 "parser.y"
                                      { if (generate_code) {
                                          NEW_BNODE((yyval.boolt));
                                          (yyval.boolt)->constnode.type = cnode;
                                          (yyval.boolt)->constnode.val  = 1;
                                        }
                                      }
#line 3356 "parser.tab.c"
    break;

  case 211:
#line 1121 "parser.y"
                                        { int flags, i;
                                          flags = 0;

                                          for (i = 0; (yyvsp[0].strval)[i] != '\0'; i++) {
                                            switch ((yyvsp[0].strval)[i]) {
                                            case 'c':
                                              flags |= IGNORE_CASE;
                                              break;
                                            case 'd':
                                              flags |= IGNORE_DIAC;
                                              break;
                                            case 'l':
                                              flags = IGNORE_REGEX; /* literal */
                                              break;
                                            default:
                                              cqpmessage(Warning, "Unknown flag %s%c (ignored)", "%", (yyvsp[0].strval)[i]);
                                              break;
                                            }
                                          }

                                          /* %l supersedes all others */
                                          if (flags & IGNORE_REGEX) {
                                            if (flags != IGNORE_REGEX) {
                                              cqpmessage(Warning, "%s and %s flags cannot be combined with %s (ignored)",
                                                         "%c", "%d", "%l");
                                            }
                                            flags = IGNORE_REGEX;
                                          }

                                          (yyval.ival) = flags;
                                        }
#line 3392 "parser.tab.c"
    break;

  case 212:
#line 1152 "parser.y"
                                        { (yyval.ival) = 0; }
#line 3398 "parser.tab.c"
    break;

  case 213:
#line 1155 "parser.y"
                                       { (yyval.boolt) = (yyvsp[0].boolt); }
#line 3404 "parser.tab.c"
    break;

  case 214:
#line 1156 "parser.y"
                                       { (yyval.boolt) = NULL; }
#line 3410 "parser.tab.c"
    break;

  case 215:
#line 1161 "parser.y"
                                       { prepare_AlignmentConstraints((yyvsp[0].strval)); }
#line 3416 "parser.tab.c"
    break;

  case 216:
#line 1163 "parser.y"
                                       { if (generate_code)
                                           CurEnv->negated = (yyvsp[-1].ival);
                                       }
#line 3424 "parser.tab.c"
    break;

  case 217:
#line 1166 "parser.y"
                                       { }
#line 3430 "parser.tab.c"
    break;

  case 218:
#line 1169 "parser.y"
                                       { (yyval.ival) = 1; }
#line 3436 "parser.tab.c"
    break;

  case 219:
#line 1170 "parser.y"
                                       { (yyval.ival) = 0; }
#line 3442 "parser.tab.c"
    break;

  case 220:
#line 1174 "parser.y"
                { if (generate_code) {
                    CurEnv->search_context.direction = (yyvsp[-1].direction);
                    CurEnv->search_context.type = (yyvsp[0].context).type;
                    CurEnv->search_context.size = (yyvsp[0].context).size;
                    CurEnv->search_context.attrib = (yyvsp[0].context).attrib;
                  }
                }
#line 3454 "parser.tab.c"
    break;

  case 221:
#line 1181 "parser.y"
                                        { if (generate_code) {
                                            CurEnv->search_context.type  = word;
                                            CurEnv->search_context.size  = hard_boundary;
                                            CurEnv->search_context.attrib = NULL;
                                          }
                                        }
#line 3465 "parser.tab.c"
    break;

  case 222:
#line 1189 "parser.y"
                                        { (yyval.ival) = abs((yyvsp[0].ival)); }
#line 3471 "parser.tab.c"
    break;

  case 223:
#line 1190 "parser.y"
                                        { (yyval.ival) = 0; }
#line 3477 "parser.tab.c"
    break;

  case 224:
#line 1193 "parser.y"
                                        { (yyval.ival) = (yyvsp[0].ival); }
#line 3483 "parser.tab.c"
    break;

  case 225:
#line 1194 "parser.y"
                                        { (yyval.ival) = 1; }
#line 3489 "parser.tab.c"
    break;

  case 226:
#line 1197 "parser.y"
                                        { (yyval.ival) = (yyvsp[0].ival); }
#line 3495 "parser.tab.c"
    break;

  case 227:
#line 1198 "parser.y"
                                        { (yyval.ival) = 0; }
#line 3501 "parser.tab.c"
    break;

  case 228:
#line 1201 "parser.y"
                                        { (yyval.ival) = (yyvsp[0].ival); }
#line 3507 "parser.tab.c"
    break;

  case 229:
#line 1202 "parser.y"
                                        { (yyval.ival) = repeat_inf; }
#line 3513 "parser.tab.c"
    break;

  case 230:
#line 1206 "parser.y"
                                        { expansion.direction = (yyvsp[-2].direction);
                                          expansion.type = (yyvsp[0].context).type;
                                          expansion.size = (yyvsp[0].context).size;
                                          expansion.attrib = (yyvsp[0].context).attrib;
                                        }
#line 3523 "parser.tab.c"
    break;

  case 231:
#line 1211 "parser.y"
                                        { expansion.direction = leftright;
                                          expansion.type = word;
                                          expansion.size = 0;
                                          expansion.attrib = NULL;
                                        }
#line 3533 "parser.tab.c"
    break;

  case 232:
#line 1218 "parser.y"
                                       { (yyval.direction) = left; }
#line 3539 "parser.tab.c"
    break;

  case 233:
#line 1219 "parser.y"
                                       { (yyval.direction) = right; }
#line 3545 "parser.tab.c"
    break;

  case 234:
#line 1220 "parser.y"
                                       { (yyval.direction) = leftright; }
#line 3551 "parser.tab.c"
    break;

  case 235:
#line 1223 "parser.y"
                                       { do_Description(&((yyval.context)), (yyvsp[-1].ival), (yyvsp[0].strval)); }
#line 3557 "parser.tab.c"
    break;

  case 236:
#line 1224 "parser.y"
                                       { do_Description(&((yyval.context)), (yyvsp[0].ival), NULL); }
#line 3563 "parser.tab.c"
    break;

  case 237:
#line 1227 "parser.y"
                                        { (yyval.cl) = (yyvsp[0].cl); }
#line 3569 "parser.tab.c"
    break;

  case 238:
#line 1228 "parser.y"
                                        { (yyval.cl) = findcorpus("Last", UNDEF, 0); }
#line 3575 "parser.tab.c"
    break;

  case 239:
#line 1232 "parser.y"
                                        { CorpusList *cl;

                                          cqpmessage(Message, "CID: %s", (yyvsp[0].strval));

                                          if ((cl = findcorpus((yyvsp[0].strval), UNDEF, 1)) == NULL) {
                                            cqpmessage(Error,
                                                       "Corpus ``%s'' is undefined", (yyvsp[0].strval));
                                            generate_code = 0;
                                            (yyval.cl) = NULL;
                                          }
                                          else if (!access_corpus(cl)) {
                                            cqpmessage(Warning,
                                                       "Corpus ``%s'' can't be accessed", (yyvsp[0].strval));
                                            (yyval.cl) = NULL;
                                          }
                                          else
                                            (yyval.cl) = cl;
                                        }
#line 3598 "parser.tab.c"
    break;

  case 240:
#line 1252 "parser.y"
                                        { (yyval.boolt) = bool_implies((yyvsp[-2].boolt), (yyvsp[0].boolt)); }
#line 3604 "parser.tab.c"
    break;

  case 241:
#line 1253 "parser.y"
                                    { (yyval.boolt) = bool_or((yyvsp[-2].boolt), (yyvsp[0].boolt)); }
#line 3610 "parser.tab.c"
    break;

  case 242:
#line 1254 "parser.y"
                                    { (yyval.boolt) = bool_and((yyvsp[-2].boolt), (yyvsp[0].boolt)); }
#line 3616 "parser.tab.c"
    break;

  case 243:
#line 1255 "parser.y"
                                    { (yyval.boolt) = (yyvsp[-1].boolt); }
#line 3622 "parser.tab.c"
    break;

  case 244:
#line 1256 "parser.y"
                                    { (yyval.boolt) = bool_not((yyvsp[0].boolt)); }
#line 3628 "parser.tab.c"
    break;

  case 245:
#line 1257 "parser.y"
                                    { (yyval.boolt) = (yyvsp[0].boolt); }
#line 3634 "parser.tab.c"
    break;

  case 246:
#line 1260 "parser.y"
                                  { (yyval.boolt) = do_RelExpr((yyvsp[-2].boolt), (yyvsp[-1].boolo), (yyvsp[0].boolt)); }
#line 3640 "parser.tab.c"
    break;

  case 247:
#line 1262 "parser.y"
              {
                 if ((yyvsp[-2].ival) & OP_NOT) {
                   (yyval.boolt) = do_RelExpr((yyvsp[-3].boolt), cmp_neq, do_mval_string((yyvsp[-1].strval), (yyvsp[-2].ival), (yyvsp[0].ival)));
                 }
                 else {
                   (yyval.boolt) = do_RelExpr((yyvsp[-3].boolt), cmp_eq,  do_mval_string((yyvsp[-1].strval), (yyvsp[-2].ival), (yyvsp[0].ival)));
                 }
              }
#line 3653 "parser.tab.c"
    break;

  case 248:
#line 1270 "parser.y"
                                  { (yyval.boolt) = do_RelExExpr((yyvsp[0].boolt)); }
#line 3659 "parser.tab.c"
    break;

  case 249:
#line 1273 "parser.y"
                                    {(yyval.ival) = OP_CONTAINS | (yyvsp[-1].ival);}
#line 3665 "parser.tab.c"
    break;

  case 250:
#line 1274 "parser.y"
                                    {(yyval.ival) = OP_MATCHES  | (yyvsp[-1].ival);}
#line 3671 "parser.tab.c"
    break;

  case 251:
#line 1277 "parser.y"
                              {(yyval.ival) = OP_NOT;}
#line 3677 "parser.tab.c"
    break;

  case 252:
#line 1278 "parser.y"
                              {(yyval.ival) = 0;}
#line 3683 "parser.tab.c"
    break;

  case 253:
#line 1281 "parser.y"
                                        { (yyval.boolt) = do_LabelReference((yyvsp[0].strval), 0); }
#line 3689 "parser.tab.c"
    break;

  case 254:
#line 1282 "parser.y"
                                        { (yyval.boolt) = do_LabelReference((yyvsp[0].strval), 1); }
#line 3695 "parser.tab.c"
    break;

  case 255:
#line 1283 "parser.y"
                                        { (yyval.boolt) = do_IDReference((yyvsp[0].strval), 0); }
#line 3701 "parser.tab.c"
    break;

  case 256:
#line 1284 "parser.y"
                                        { (yyval.boolt) = do_IDReference((yyvsp[0].strval), 1); }
#line 3707 "parser.tab.c"
    break;

  case 257:
#line 1285 "parser.y"
                        { (yyval.boolt) = do_IDReference(cl_strdup(field_type_to_name((yyvsp[0].field))), 0); }
#line 3713 "parser.tab.c"
    break;

  case 258:
#line 1286 "parser.y"
                                        { (yyval.boolt) = (yyvsp[0].boolt); }
#line 3719 "parser.tab.c"
    break;

  case 259:
#line 1289 "parser.y"
                                        { (yyval.boolt) = (yyvsp[0].boolt); }
#line 3725 "parser.tab.c"
    break;

  case 260:
#line 1290 "parser.y"
                                        { (yyval.boolt) = do_flagged_string((yyvsp[-1].strval), (yyvsp[0].ival)); }
#line 3731 "parser.tab.c"
    break;

  case 261:
#line 1292 "parser.y"
                                        {
                                          (yyval.boolt) = do_flagged_re_variable((yyvsp[-2].strval), (yyvsp[0].ival));
                                        }
#line 3739 "parser.tab.c"
    break;

  case 262:
#line 1295 "parser.y"
                                        { if (generate_code) {
                                            if (!FindVariable((yyvsp[0].strval))) {
                                              cqpmessage(Error,
                                                         "%s: no such variable",
                                                         (yyvsp[0].strval));
                                              generate_code = 0;
                                              (yyval.boolt) = NULL;
                                            }
                                            else {
                                              NEW_BNODE((yyval.boolt));
                                              (yyval.boolt)->type = var_ref;
                                              (yyval.boolt)->varref.varName = (yyvsp[0].strval);
                                            }
                                          }
                                          else
                                            (yyval.boolt) = NULL;
                                        }
#line 3761 "parser.tab.c"
    break;

  case 263:
#line 1312 "parser.y"
                                        { if (generate_code) {
                                            NEW_BNODE((yyval.boolt));
                                            (yyval.boolt)->type = int_leaf;
                                            (yyval.boolt)->leaf.ctype.iconst = (yyvsp[0].ival);
                                          }
                                          else
                                            (yyval.boolt) = NULL;
                                        }
#line 3774 "parser.tab.c"
    break;

  case 264:
#line 1320 "parser.y"
                                        { if (generate_code) {
                                            NEW_BNODE((yyval.boolt));
                                            (yyval.boolt)->type = float_leaf;
                                            (yyval.boolt)->leaf.ctype.fconst = (yyvsp[0].fval);
                                          }
                                          else
                                            (yyval.boolt) = NULL;
                                        }
#line 3787 "parser.tab.c"
    break;

  case 265:
#line 1330 "parser.y"
                                        { (yyval.boolo) = cmp_lt; }
#line 3793 "parser.tab.c"
    break;

  case 266:
#line 1331 "parser.y"
                                        { (yyval.boolo) = cmp_gt; }
#line 3799 "parser.tab.c"
    break;

  case 267:
#line 1332 "parser.y"
                                        { (yyval.boolo) = cmp_eq; }
#line 3805 "parser.tab.c"
    break;

  case 268:
#line 1333 "parser.y"
                                        { (yyval.boolo) = cmp_neq; }
#line 3811 "parser.tab.c"
    break;

  case 269:
#line 1334 "parser.y"
                                        { (yyval.boolo) = cmp_let; }
#line 3817 "parser.tab.c"
    break;

  case 270:
#line 1335 "parser.y"
                                        { (yyval.boolo) = cmp_get; }
#line 3823 "parser.tab.c"
    break;

  case 271:
#line 1338 "parser.y"
                                            { (yyval.boolt) = FunctionCall((yyvsp[-3].strval), (yyvsp[-1].apl)); }
#line 3829 "parser.tab.c"
    break;

  case 272:
#line 1341 "parser.y"
                                            { (yyval.apl) = (yyvsp[0].apl);
                                            }
#line 3836 "parser.tab.c"
    break;

  case 273:
#line 1344 "parser.y"
                                            { ActualParamList *last;

                                              if (generate_code) {
                                                assert((yyvsp[-2].apl) != NULL);

                                                last = (yyvsp[-2].apl);
                                                while (last->next != NULL)
                                                  last = last->next;
                                                last->next = (yyvsp[0].apl);
                                                (yyval.apl) = (yyvsp[-2].apl);
                                              }
                                              else
                                                (yyval.apl) = NULL;
                                            }
#line 3855 "parser.tab.c"
    break;

  case 274:
#line 1360 "parser.y"
                                         { if (generate_code) {
                                             New((yyval.apl), ActualParamList);

                                             (yyval.apl)->param = (yyvsp[0].boolt);
                                             (yyval.apl)->next = NULL;
                                           }
                                           else
                                             (yyval.apl) = NULL;
                                         }
#line 3869 "parser.tab.c"
    break;

  case 275:
#line 1371 "parser.y"
                                        { (yyval.strval) = (yyvsp[0].strval); }
#line 3875 "parser.tab.c"
    break;

  case 276:
#line 1374 "parser.y"
                                        { (yyval.evalt) = (yyvsp[0].evalt); }
#line 3881 "parser.tab.c"
    break;

  case 277:
#line 1375 "parser.y"
                                        { (yyval.evalt) = (yyvsp[0].evalt); }
#line 3887 "parser.tab.c"
    break;

  case 278:
#line 1376 "parser.y"
                                        { if (generate_code) {
                                            NEW_EVALLEAF((yyval.evalt), (yyvsp[0].index));
                                          }
                                          else
                                            (yyval.evalt) = NULL;
                                        }
#line 3898 "parser.tab.c"
    break;

  case 279:
#line 1388 "parser.y"
                                        { (yyval.evalt) = do_MeetStatement((yyvsp[-3].evalt), (yyvsp[-2].evalt), &((yyvsp[-1].context))); }
#line 3904 "parser.tab.c"
    break;

  case 280:
#line 1392 "parser.y"
                                        { (yyval.context).type = word;
                                          (yyval.context).size = (yyvsp[-1].ival);
                                          (yyval.context).size2 = (yyvsp[0].ival);
                                          (yyval.context).attrib = NULL;
                                        }
#line 3914 "parser.tab.c"
    break;

  case 281:
#line 1397 "parser.y"
                                        { do_StructuralContext(&((yyval.context)), (yyvsp[0].strval)); }
#line 3920 "parser.tab.c"
    break;

  case 282:
#line 1398 "parser.y"
                                        { (yyval.context).type = word;
                                          (yyval.context).size = 1;
                                          (yyval.context).size2 = 1;
                                          (yyval.context).attrib = NULL;
                                        }
#line 3930 "parser.tab.c"
    break;

  case 283:
#line 1409 "parser.y"
                                        { (yyval.evalt) = do_UnionStatement((yyvsp[-2].evalt), (yyvsp[-1].evalt)); }
#line 3936 "parser.tab.c"
    break;

  case 284:
#line 1414 "parser.y"
                                        { (yyval.cl) = do_TABQuery((yyvsp[-1].evalt)); }
#line 3942 "parser.tab.c"
    break;

  case 285:
#line 1419 "parser.y"
                                        { (yyval.evalt) = make_first_tabular_pattern((yyvsp[-1].index), (yyvsp[0].evalt)); }
#line 3948 "parser.tab.c"
    break;

  case 286:
#line 1424 "parser.y"
                                        { (yyval.evalt) = add_tabular_pattern((yyvsp[-2].evalt), &((yyvsp[-1].context)), (yyvsp[0].index)); }
#line 3954 "parser.tab.c"
    break;

  case 287:
#line 1426 "parser.y"
                                        { (yyval.evalt) = NULL; }
#line 3960 "parser.tab.c"
    break;

  case 288:
#line 1429 "parser.y"
                                        { do_OptDistance(&((yyval.context)), (yyvsp[-1].ival), (yyvsp[-1].ival)); }
#line 3966 "parser.tab.c"
    break;

  case 289:
#line 1431 "parser.y"
                                        { do_OptDistance(&((yyval.context)), (yyvsp[-3].ival), (yyvsp[-1].ival)); }
#line 3972 "parser.tab.c"
    break;

  case 290:
#line 1432 "parser.y"
                                        { do_OptDistance(&((yyval.context)), 0, (yyvsp[-1].ival)); }
#line 3978 "parser.tab.c"
    break;

  case 291:
#line 1433 "parser.y"
                                        { do_OptDistance(&((yyval.context)), 0, 0); }
#line 3984 "parser.tab.c"
    break;

  case 292:
#line 1438 "parser.y"
                                        { add_user_to_list((yyvsp[-1].strval), (yyvsp[0].strval)); }
#line 3990 "parser.tab.c"
    break;

  case 294:
#line 1440 "parser.y"
                                        { add_host_to_list((yyvsp[0].strval)); }
#line 3996 "parser.tab.c"
    break;

  case 295:
#line 1441 "parser.y"
                                        { add_hosts_in_subnet_to_list((yyvsp[0].strval)); }
#line 4002 "parser.tab.c"
    break;

  case 296:
#line 1442 "parser.y"
                                        { add_host_to_list(NULL); }
#line 4008 "parser.tab.c"
    break;

  case 299:
#line 1451 "parser.y"
                                        { add_grant_to_last_user((yyvsp[0].strval)); }
#line 4014 "parser.tab.c"
    break;

  case 301:
#line 1457 "parser.y"
                                                 {
                                                if (enable_macros)
                                                  define_macro((yyvsp[-4].strval), (yyvsp[-2].ival), NULL, (yyvsp[0].strval));  /* <macro.c> */
                                                else
                                                  cqpmessage(Error, "CQP macros not enabled.");
                                                free((yyvsp[0].strval));  /* don't forget to free the allocated strings */
                                                free((yyvsp[-4].strval));
                                                }
#line 4027 "parser.tab.c"
    break;

  case 302:
#line 1466 "parser.y"
                                                {
                                                if (enable_macros)
                                                  define_macro((yyvsp[-4].strval), 0, (yyvsp[-2].strval), (yyvsp[0].strval));  /* <macro.c> */
                                                else
                                                  cqpmessage(Error, "CQP macros not enabled.");
                                                free((yyvsp[0].strval));  /* don't forget to free the allocated strings */
                                                free((yyvsp[-2].strval));
                                                free((yyvsp[-4].strval));
                                                }
#line 4041 "parser.tab.c"
    break;

  case 303:
#line 1475 "parser.y"
                                                     {
                                                load_macro_file((yyvsp[0].strval));
                                                free((yyvsp[0].strval));  /* don't forget to free the allocated string */
                                                }
#line 4050 "parser.tab.c"
    break;

  case 306:
#line 1486 "parser.y"
                                        {
                                          list_macros(NULL);
                                        }
#line 4058 "parser.tab.c"
    break;

  case 307:
#line 1489 "parser.y"
                                        {
                                          list_macros((yyvsp[0].strval));
                                          free((yyvsp[0].strval));
                                        }
#line 4067 "parser.tab.c"
    break;

  case 308:
#line 1494 "parser.y"
                                        {
                                          print_macro_definition((yyvsp[-3].strval), (yyvsp[-1].ival));
                                          free((yyvsp[-3].strval));
                                        }
#line 4076 "parser.tab.c"
    break;

  case 309:
#line 1502 "parser.y"
                                        {
                                          int l1 = strlen((yyvsp[-1].strval)), l2 = strlen((yyvsp[0].strval));
                                          char *s = (char *) cl_malloc(l1 + l2 + 2);
                                          strcpy(s, (yyvsp[-1].strval)); s[l1] = ' ';
                                          strcpy(s+l1+1, (yyvsp[0].strval));
                                          s[l1+l2+1] = '\0';
                                          free((yyvsp[-1].strval));
                                          free((yyvsp[0].strval));
                                          (yyval.strval) = s;
                                        }
#line 4091 "parser.tab.c"
    break;

  case 310:
#line 1512 "parser.y"
                                        { (yyval.strval) = (yyvsp[0].strval); }
#line 4097 "parser.tab.c"
    break;

  case 311:
#line 1516 "parser.y"
                                        { cl_randomize(); }
#line 4103 "parser.tab.c"
    break;

  case 312:
#line 1517 "parser.y"
                                        { cl_set_seed((yyvsp[0].ival)); }
#line 4109 "parser.tab.c"
    break;

  case 314:
#line 1525 "parser.y"
                                        { (yyval.field) = (yyvsp[0].field); }
#line 4115 "parser.tab.c"
    break;

  case 315:
#line 1526 "parser.y"
                                        { (yyval.field) = NoField; }
#line 4121 "parser.tab.c"
    break;

  case 324:
#line 1542 "parser.y"
                                        { (yyval.Anchor).anchor = (yyvsp[0].field); (yyval.Anchor).offset = 0; }
#line 4127 "parser.tab.c"
    break;

  case 325:
#line 1543 "parser.y"
                                        { (yyval.Anchor).anchor = (yyvsp[-3].field); (yyval.Anchor).offset = (yyvsp[-1].ival); }
#line 4133 "parser.tab.c"
    break;


#line 4137 "parser.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1546 "parser.y"


