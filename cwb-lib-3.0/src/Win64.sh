source build_for_txm.sh

sudo apt-get install --force-yes binutils-mingw-w64-i686 binutils-mingw-w64-x86-64 gcc-mingw-w64-i686 gcc-mingw-w64-x86-64 gcc-mingw-w64 &&
sudo cp /usr/include/paths.h /usr/lib/gcc/x86_64-w64-mingw32/9.3-win32/include/paths.h

  if [ $? != 0 ]; then
	echo "** FAIL !!!!"
	exit 1;
  fi

# Windows 64
patchConfig "mingw64" &&
buildCWB "mingw64" &&
copyBuilds "mingw64"

if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to copy binaries for Windows 64"
	exit 1;
fi

