function patchConfig() {
arch=$1
sed "s/^include \$(TOP)\/config\/platform\/.*$/include \$(TOP)\/config\/platform\/$arch/g" config.mk > tmp &&
rm "config.mk" &&
mv tmp "config.mk"

  if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to patch config.mk"
	exit 1;
  fi
} 

function buildCWB() {
# $1: arch
  make clean && make depend
echo "????????"
  if [ "$1" = "darwin-universal" ]; then
    echo "fixing depend.mk"
pwd
ls */depend.mk
    find */depend.mk -type f -exec perl -p -i -e 's/glib.h//g' {} \;
    find */depend.mk -type f -exec perl -p -i -e 's/pcre.h//g' {} \;
    find */depend.mk -type f -exec perl -p -i -e 's/glib\/gstdio.h//g' {} \;
  fi

  make all

  if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to compile for arch $1"
	exit 1;
  fi
}

function copyBuilds() {
	# $1: arch
	mkdir "builds/$1/"

if [ "$1" = "mingw" -o "$1" = "mingw64" ]; then
	cp "cqp/cqpserver.exe" "builds/$1/" &&
	cp "cqp/cqp.exe" "builds/$1/" &&
	cp "cqp/cqpjni.dll" "builds/$1/" &&
	cp "utils/cwb-align.exe" "builds/$1/" &&
	cp "utils/cwb-align-encode.exe" "builds/$1/" &&
	cp "utils/cwb-encode.exe" "builds/$1/" &&
	cp "utils/cwb-decode.exe" "builds/$1/" &&
	cp "utils/cwb-makeall.exe" "builds/$1/"
elif [ "$1" = "darwin-universal" ]; then
        cp "cqp/cqpserver" "builds/$1/" &
        cp "cqp/cqp" "builds/$1/" &&
        cp "cqp/libcqpjni.dylib" "builds/$1/" &&
        cp "utils/cwb-align" "builds/$1/" &&
        cp "utils/cwb-align-encode" "builds/$1/" &&
        cp "utils/cwb-encode" "builds/$1/" &&
        cp "utils/cwb-decode" "builds/$1/" &&
        cp "utils/cwb-makeall" "builds/$1/"
else
	cp "cqp/cqpserver" "builds/$1/" &&
	cp "cqp/cqp" "builds/$1/" &&
	cp "cqp/libcqpjni.so" "builds/$1/" &&
	cp "utils/cwb-align" "builds/$1/" &&
	cp "utils/cwb-align-encode" "builds/$1/" &&
	cp "utils/cwb-encode" "builds/$1/" &&
	cp "utils/cwb-decode" "builds/$1/" &&
	cp "utils/cwb-huffcode" "builds/$1/" &&
	cp "utils/cwb-compress-rdx" "builds/$1/" &&
	cp "utils/cwb-makeall" "builds/$1/"
fi
		
	if [ $? != 0 ]; then
		echo "** buildCWB4All: failed to copy binaries for $arch"
		exit 1;
	fi
}
