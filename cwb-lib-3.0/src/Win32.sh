source build_for_txm.sh

sudo apt-get install --force-yes mingw32 mingw32-runtime mingw32-binutils &&
sudo cp /usr/include/paths.h /usr/lib/gcc/i586-mingw32msvc/4.2.1-sjlj/include

  if [ $? != 0 ]; then
	echo "** FAIL !!!!"
	exit 1;
  fi

# Windows 32
patchConfig "mingw" &&
buildCWB "mingw" &&
copyBuilds "mingw"

if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to copy binaries for Windows 32"
	exit 1;
fi

