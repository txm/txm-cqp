source build_for_txm.sh

# Linux64
sudo apt-get install --force-yes bison flex libglib2.0-dev libncurses5-dev libreadline-dev openjdk-7-jdk

patchConfig "linux-64" &&
buildCWB "linux-64" &&
copyBuilds "linux-64"

if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to copy binaries for LINUX 32"
	exit 1;
fi

