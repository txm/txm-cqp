source build_for_txm.sh

# macports dependancies:
# glib2-devel
# bzip2
# gettext
# libedit
# libffi
# libiconv
# ncurses
# pcre
# zlib

# MacOSX
patchConfig "darwin-universal" &&
buildCWB "darwin-universal" &&
copyBuilds "darwin-universal"


if [ $? != 0 ]; then
	echo "** buildCWB4All: failed to copy binaries for Mac OS X"
	exit 1;
fi
